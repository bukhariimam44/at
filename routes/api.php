<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/daftarsekolah', ['as'=>'daftarsekolah','uses'=>'CallbackController@daftarsekolah']);

Route::get('/v1/km_callback','CallbackController@callback');
Route::post('/v1/km_callback','CallbackController@callback');

Route::group(['prefix'=>'at-api/v1/'], function()
{
   Route::post('login', ['as'=>'api-login','uses'=>'Api\AuthController@login']);
   Route::post('register', ['as'=>'api-register','uses'=>'Api\AuthController@register']);
});

Route::group(['prefix'=>'at-api/v1/','middleware'=>'auth:api'], function()
{
    Route::get('data-harga', ['as'=>'api-data-harga','uses'=>'Api\UserController@data_harga']);
    Route::get('cek-pln-psc', ['as'=>'cek-pln-psc','uses'=>'Api\PlnPscController@plnpsc']);
    Route::post('proses-pulsa', ['as'=>'proses-pulsa','uses'=>'Api\ProsesPpobController@proses_pulsa']);
    Route::post('proses-pln', ['as'=>'proses-pln','uses'=>'Api\ProsesPpobController@proses_pln']);
    Route::post('proses-game', ['as'=>'proses-game','uses'=>'Api\ProsesPpobController@proses_game']);
    Route::post('proses-e-money', ['as'=>'proses-e-money','uses'=>'Api\ProsesPpobController@proses_e_money']);
				Route::post('cek-status-ppob', ['as'=>'cek-status-ppob','uses'=>'Api\CekStatusController@cek_status_ppob']);
				//TAGIHAN PASCABAYAR
				Route::get('kategori_pascabayar', ['as'=>'kategori_pascabayar','uses'=>'Api\UserController@kategori_pascabayar']);
				Route::post('pembayaran_product', ['as'=>'pembayaran_product','uses'=>'Api\UserController@pembayaran_product']);
				Route::post('cek_tagihan', ['as'=>'cek_tagihan','uses'=>'Api\UserController@cek_tagihan']);
				Route::post('bayar_tagihan', ['as'=>'bayar_tagihan','uses'=>'Api\UserController@bayar_tagihan']);

				//DEPOSIT
			Route::get('data-bank', ['as'=>'api-data-bank','uses'=>'Api\DepositController@data_bank']);
			Route::post('proses-topup', ['as'=>'proses-topup','uses'=>'Api\DepositController@proses_topup']);
			Route::post('buku-saldo', ['as'=>'buku-saldo','uses'=>'Api\DepositController@buku_saldo']);

});
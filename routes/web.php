<?php
Route::get('liat-log-master', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('admin');
// Route::get('/call', ['as'=>'call','uses'=>'AuthController@call'])->middleware('guest');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', ['as'=>'login','uses'=>'AuthController@masuk'])->middleware('guest');
Route::post('/login', ['as'=>'login','uses'=>'AuthController@masuk']);
Route::get('/', ['as'=>'home','uses'=>'AdminController@home'])->middleware('admin');
Route::get('/harga-product-{id}', ['as'=>'harga-product','uses'=>'AdminController@harga_product'])->middleware('admin');
Route::post('/harga-product-{id}', ['as'=>'harga-product','uses'=>'AdminController@harga_product'])->middleware('admin');
//DEPOSIT
Route::get('/request-topup', ['as'=>'request-topup','uses'=>'AdminController@request_topup'])->middleware('admin');
Route::post('/proses-topup', ['as'=>'proses-topup','uses'=>'AdminController@proses_topup'])->middleware('admin');
Route::post('/cancel-topup', ['as'=>'cancel-topup','uses'=>'AdminController@cancel_topup'])->middleware('admin');
Route::get('/buku-saldo-agen', ['as'=>'buku-saldo-agen','uses'=>'AdminController@buku_saldo_agen'])->middleware('admin');
Route::post('/buku-saldo-agen', ['as'=>'buku-saldo-agen','uses'=>'AdminController@buku_saldo_agen'])->middleware('admin');
Route::get('/topup-manual', ['as'=>'topup-manual','uses'=>'AdminController@topup_manual'])->middleware('admin');
Route::post('/topup-manual', ['as'=>'topup-manual','uses'=>'AdminController@topup_manual'])->middleware('admin');
//TRANSFER
Route::get('/request-transfer', ['as'=>'request-transfer','uses'=>'AdminController@request_transfer'])->middleware('admin');

//
Route::get('/data-transaksi-ppob', ['as'=>'data-transaksi-ppob','uses'=>'AdminController@data_transaksi_ppob'])->middleware('admin');
Route::post('/data-transaksi-ppob', ['as'=>'data-transaksi-ppob','uses'=>'AdminController@data_transaksi_ppob'])->middleware('admin');
Route::post('/cek-status', ['as'=>'cek-status','uses'=>'AdminController@cek_status'])->middleware('admin');
Route::get('/laporan-transaksi-ppob', ['as'=>'laporan-transaksi-ppob','uses'=>'AdminController@laporan_transaksi_ppob'])->middleware('admin');
Route::post('/laporan-transaksi-ppob', ['as'=>'laporan-transaksi-ppob','uses'=>'AdminController@laporan_transaksi_ppob'])->middleware('admin');

Route::get('/update-harga-agen', ['as'=>'update-harga-agen','uses'=>'AdminController@update_harga_agen'])->middleware('admin');

// Route::get('/update-harga-pln', ['as'=>'update-harga-pln','uses'=>'HargaController@update_harga']);
Route::get('/logout', ['as'=>'logout','uses'=>'AdminController@logout']);

// Route::post('/callback','AuthController@callback');
Route::get('/update_ppob_6244/{produk}', ['as'=>'update_ppob_6244','uses'=>'UpdateAddPpob@update_ppob'])->middleware('admin');


Route::get('/testing-api', ['as'=>'testing-api','uses'=>'TestingController@testing_api'])->middleware('admin');

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DataHarga;

use App\Helpers\Req;
use Log;
use DB;

class HargaLara extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:harga_lara';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
							DB::beginTransaction();
							try {
								$data = [];
								$categorie = Req::postLara($data,$this->supplier = 'larakostpulsa',$add_url = 'pembelian/categorie')->get();
								if ($categorie['status'] == 'success') {
									foreach ($categorie['data'] as $key => $categori) {
										sleep(1);
										$data = array(
											'product_id' => $categori['product_id'], // Kode Kategori (pulsa, plnpra, paket, dll)
										);
										$operators = Req::postLara($data,$this->supplier = 'larakostpulsa',$add_url = 'pembelian/operator')->get();
										if ($operators['status'] == 'success') {
											foreach ($operators['data'] as $key => $operator) {
												sleep(1);
												$data = array(
													'product_id' => $operator['product_id'], // Kode Kategori (pulsa, plnpra, paket, dll)
												);
												$products = Req::postLara($data,$this->supplier = 'larakostpulsa',$add_url = 'pembelian/product')->get();
												if ($products['status'] == 'success') {
													foreach ($products['data'] as $key => $product) {
														if ($update = DataHarga::where('code_lara',$product['product_id'])->where('supplier','=','larakostpulsa')->first()) {
																				$update->code_lara = $product['product_id'];
																				// $update->description = $product['product_name'];
																				$update->price = $product['h2h_price'];
																				if ($product['status'] == 1) {
																					$update->status = 'normal';
																				}else {
																					$update->status = 'gangguan';
																				}
																				$update->update();
															}
													}
												}
											}
										}
									}
									
								}
								
							} catch (\Throwable $th) {
								Log::info('Gagal Update Harga lara:'.$th->getMessage());
								DB::rollback();
								return response()->json([
									'code'=>400
								]);
							}
							DB::commit();
							Log::info('Berhasil Update Harga lara');
							return response()->json([
								'code'=>200
							]);
    }
}

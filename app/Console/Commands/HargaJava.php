<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DataHarga;

use App\Helpers\Req;
use Log;
use DB;

class HargaJava extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:harga_java';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Harga Java';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
					$data = array(
									'inquiry' => 'HARGA', // konstan
									'code' => 'pulsa', // pilihan: pln, pulsa, game
					);
					$result = Req::post($data,'javah2h')->get();
					foreach ($result['message'] as $key => $value) {
						if ($update = DataHarga::where('code',$value['code'])->where('supplier','=','javah2h')->first()) {
											$update->code = $value['code'];
											// $update->description = $value['description'];
											$update->price = $value['price'];
											$update->status = $value['status'];
											$update->update();
										}
						}
    }
}

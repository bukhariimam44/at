<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataTransaksiPpob extends Model
{
    protected $fillable = [
        'user_id','no_trx','tgl_trx','code','on_check', 'trxid_api','url_agen','supplier', 'idcust','phone','sequence','status_ppob_id','sn','note','price','jual','last_balance'
				];
				public function statusPpobId(){
					return $this->belongsTo('App\StatusPpob','status_ppob_id');
				}
				public function userId(){
					return $this->belongsTo('App\User','user_id');
				}
}

<?php
namespace App\Helpers\API;

use JavaScript;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Log;

/**
 *
 */
class Send extends Api
{
  public function __construct($data, $supplier,$add_url,$headers = [])
  {
      parent::__construct();
      if ($supplier == 'portalpulsa') {
        $this->url=config('ppob-config')['portal_url'];
        $this->userid = config('ppob-config')['portal_userid'];
        $this->key = config('ppob-config')['portal_key'];
        $this->secret = config('ppob-config')['portal_secret'];
      }elseif($supplier == 'javah2h') {
        $this->url=config('ppob-config')['h2h_url'];
        $this->userid = config('ppob-config')['h2h_userid'];
        $this->key = config('ppob-config')['h2h_key'];
        $this->secret = config('ppob-config')['h2h_secret'];
      }else {
							$this->url=config('ppob-config')['url_larakostpulsa'].$add_url;
							$this->userid = config('ppob-config')['userid_larakostpulsa'];
							$this->key=config('ppob-config')['key_larakostpulsa'];
						}
      
      $this->data = $data;
      $this->header = $this->getHeaders($headers);
      $this->getData();
  }

  private function getHeaders($headers){
    if ($this->userid == 'H0136') {
      $head = array(
        'h2h-userid: '.$this->userid,
        'h2h-key: '.$this->key, // lihat hasil autogenerate di member area
        'h2h-secret: '.$this->secret, // lihat hasil autogenerate di member area
      );
    }elseif($this->userid == 'P108424') {
      $head = array(
        'portal-userid: '.$this->userid,
        'portal-key: '.$this->key, // lihat hasil autogenerate di member area
        'portal-secret: '.$this->secret, // lihat hasil autogenerate di member area
      );
    }else {
						$head = array(
							'Accept: application/json',
							'Authorization: Bearer '.$this->key, // Ganti [apikey] dengan API KEY Anda
						);
				}
     
    return $head;
  }
}
<?php

namespace App\Helpers\API;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

abstract class Api
{
    protected $url;
    protected $userid;
    protected $key;
    protected $secret;
    protected $data;
    protected $header;
    

    public function __construct()
    {
   
    }

    public function get(){
        return $this->data;
    }

    protected function getData(){
        try {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $this->url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
          $result = curl_exec($ch);
          $response = json_decode($result, true);
        } catch (ClientException $e) {
          $response = json_decode($result, true);
          $this->data = $response;
          return $this;
								}
								if (isset($response['result'])) {
									if($response['result'] == 'success') {
											Log::info('DATA RESUL SUCCESS NONLARA= '.$result);
											$this->data = $response;
											return $this;
									}
									$this->data= $response;
        	return $this;
								}else {
									if($response['status'] == 'success') {
											Log::info('DATA RESUL SUCCESS LARA = '.$result);
											$this->data = $response;
											return $this;
									}
									$this->data= $response;
        	return $this;
								}
        $this->data= $response;
        return $this;
    }
}
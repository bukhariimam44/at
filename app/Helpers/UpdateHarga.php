<?php

namespace App\Helpers;


use App\Helpers\API\Send;
use Log;

class UpdateHarga
{
    public static function ganti($data){
      $result =  new Send($data);
      Log::info('ISI RESUL :'.json_encode($result));
      foreach ($result['message'] as $key => $value) {
            DataHarga::create([
                'provider'=>$value['provider'],
                'provider_sub'=>$value['provider_sub'],
                'operator'=>$value['operator'],
                'operator_sub'=>$value['operator_sub'],
                'code'=>$value['code'],
                'description'=>$value['description'],
                'price'=>$value['price'],
                'status'=>$value['status']
            ]);
        }
    }
}


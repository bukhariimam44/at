<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryTransaksiPascabayar extends Model
{
	protected $fillable = [
		'inquiry_id','user_id','fee','jml_bayar','fee_agen','jml_bayar_agen','tagihan_id','code','kategori','no_pelanggan','periode','nama',	'jumlah_bulan','jumlah_tagihan','admin','jumlah_bayar','detail','status','aktif'
	];
}

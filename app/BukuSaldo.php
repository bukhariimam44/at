<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuSaldo extends Model
{
    protected $fillable = [
        'id','user_id', 'no_trx','mutasi','nominal','saldo_akhir','keterangan'
    ];
}

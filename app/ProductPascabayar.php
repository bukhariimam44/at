<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPascabayar extends Model
{
	public $incrementing = false;
	protected $fillable = [
		'id','kategori_pascabayar_id','product_name','fee', 'status','created_at','updated_at'
	];
}

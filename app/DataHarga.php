<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataHarga extends Model
{
    protected $fillable = [
        'provider','provider_sub','operator', 'operator_sub', 'code','supplier','description','price','markup','status'
    ];
}

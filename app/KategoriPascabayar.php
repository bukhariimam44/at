<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPascabayar extends Model
{
	public $incrementing = false;
	protected $fillable = [
		'id','product_name', 'status','created_at','updated_at'
	];
}

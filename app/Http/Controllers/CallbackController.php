<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\DataHarga;
use App\DataTransaksiPpob;
use App\BukuSaldo;
use App\BankSupplier;
use App\DepositAgen;
use Auth;
use Log;
use DB;

class CallbackController extends Controller
{

	public function __construct()
	{
					$this->sekolah = config('ppob-config')['sekolah'];
					$this->token = config('ppob-config')['token'];
	}
			public function callback(Request $request){
				Log::info('REMOTE_ADDR LUAR: '.$_SERVER['REMOTE_ADDR']);
					$callback = json_decode($request->content,true);
					Log::info('CALLBACK LUAR: '.json_encode($callback));

				if($_SERVER['REMOTE_ADDR']=='172.104.161.223'){ // memastikan data terikirim dari server portalpulsa
						// $callback = $request->all();
								$callback = json_decode($request->content,true);
								Log::info('CALLBACK JAVA: '.json_encode($callback));
								DB::beginTransaction();
								try {
												if (!$cek = DataTransaksiPpob::where('trxid_api',$callback['trxid_api'])->where('on_check','=',0)->first()) {
													Log::info('DALAM AUTO CEK STATUS NON LARA: '.$callback['trxid_api']);
													return response()->json([
														'code'=>200,
														'message'=>'Telah di update'
													]);
												}
												
												$product = DataHarga::where('code',$callback['code'])->first();
												// $cek->supplier = $product->supplier;
												$cek->status_ppob_id = $callback['status'];
												$cek->idcust = $callback['idcust'];
												$cek->note = $callback['note'];
												$note = $callback['note'];
												if ($callback['status'] == 4) {
																$user = User::find($cek->user_id);
																$cek->sn = $callback['sn'];
																$cek->last_balance = $callback['last_balance'];
																$saldo_akhir = $user->saldo;
												}else{
																$user = User::find($cek->user_id);
																$saldo_akhir = (int)($user->saldo + $cek->jual);
																$user->saldo = (int)$saldo_akhir;
																$user->update();
																BukuSaldo::create([
																				'user_id'=>$user->id,
																				'no_trx'=>date('ymdhis').$cek->user_id,
																				'mutasi'=>'Kredit',
																				'nominal'=>(int)$cek->jual,
																				'saldo_akhir'=>(int)$saldo_akhir,
																				'keterangan'=>'Refund Gagal Transaksi '.$callback['trxid_api']
																]);
												}
												if (isset($callback['price'])) {
													$cek->price = (int)$callback['price'];
													if ($callback['price'] > $product->price) {
														$product->price = (int)$callback['price'];
														$product->update();
													}
												}
											
												$cek->update();

												$saldo = $user->saldo;
												$update_saldo = User::where('id',$user->id)->where('saldo','=',$saldo)->update([
																'saldo'=>$saldo_akhir
												]);
												if (!$update_saldo) {
													DB::rollback();
													Log::info('GAGAL UPDATE SALDO CALBACK :'.$callback['trxid_api']);
													return response()->json([
																	'code'=>400,
																	'message'=>'Gagal',
													]);
												}
								} catch (\Throwable $th) {
												Log::info('Gagal Update transaksi:'.$th->getMessage());
												DB::rollback();
												return response()->json([
													'code'=>400,
													'message'=>'Gagal'
												]);
								}
								DB::commit();
								$client = new \GuzzleHttp\Client();
								// if (isset($callback['last_balance'])) {
								// 	if ($callback['last_balance'] < 200000) {
								// 		$response = $client->request('GET','https://api.telegram.org/bot1344707970:AAGJFTejdmDzHwyEcjoCwl_DwOdSMRbnKdA/sendMessage?chat_id=1252367618&text=Saldo Java kamu sisa Rp '.$callback['last_balance'].', Silahkan topup lagi.');
								// 	}
								// }
								$statuses = $callback['status'];
								$no_trxes = $callback['trxid_api'];
								$code = $callback['code'];
								$url = $cek->url_agen;
								if (isset($callback['price'])) {
									if ($callback['price'] > $cek->price) {
										$price = (int)($callback['price']+$product->markup);
									}else {
										$price = (int)($cek->price+$product->markup);
									}
								}else {
									$price = (int)($cek->price+$product->markup);
								}
								if ($statuses == 4) {
									$snes = $callback['sn'];
								}else{
									$snes = null;
								}
								$array = [
										'no_trx' => $no_trxes,
										'status' => $statuses,
										'code'=> $code,
										'price'=>$price,
										'sn' => $snes,
										'note'=>$note
								];
								$response = $client->post($url , [
												'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
												'body' => json_encode($array)
								]);
								return response()->json([
												'code'=>200,
												'message'=>'Berhasil'
								]);
				}elseif($_SERVER['REMOTE_ADDR']=='139.180.142.27') {
					Log::info('REMOTE_ADDR LARA: '.$_SERVER['REMOTE_ADDR']);
					$callback = json_decode($request->content,true);
					Log::info('CALLBACK LARA: '.json_encode($callback));
					DB::beginTransaction();
								try {
												if (!$cek = DataTransaksiPpob::where('trxid_api',$callback['api_idtrx'])->where('on_check','=',0)->first()) {
													Log::info('DALAM AUTO CEK STATUS LARA: '.$callback['api_idtrx']);
													return response()->json([
														'code'=>200,
														'message'=>'Telah di update'
													]);
												}
												$product = DataHarga::where('code_lara',$callback['code'])->first();
												$cek->note = $callback['note'];
												$note = $callback['note'];
												if ($callback['status'] == 1) {
													$status = 4;
												}else {
													$status = 2;
												}
												$cek->status_ppob_id = $status;											
												if ($status == 4) {
																$user = User::find($cek->user_id);
																$cek->sn = $callback['token'];
																$cek->last_balance = $callback['saldo_after_trx'];
																$saldo_akhir = (int)($user->saldo);
												}else{
																$user = User::find($cek->user_id);
																$saldo_akhir = (int)($user->saldo + $cek->jual);
																$user->saldo = (int)$saldo_akhir;
																$user->update();
																BukuSaldo::create([
																				'user_id'=>$user->id,
																				'no_trx'=>date('ymdhis').$cek->user_id,
																				'mutasi'=>'Kredit',
																				'nominal'=>(int)$cek->jual,
																				'saldo_akhir'=>(int)$saldo_akhir,
																				'keterangan'=>'Refund Gagal Transaksi '.$callback['api_idtrx']
																]);
												}
												if (isset($callback['total'])) {
													$cek->price = (int)$callback['total'];
													if ($callback['total'] > $product->price) {
														$product->price = (int)$callback['total'];
														$product->update();
													}
												}
											
												$cek->update();
												$saldo = $user->saldo;
												$update_saldo = User::where('id',$user->id)->where('saldo','=',$saldo)->update([
																'saldo'=>$saldo_akhir
												]);
												if (!$update_saldo) {
													DB::rollback();
													Log::info('GAGAL UPDATE SALDO CALBACK LARA:'.$callback['trxid_api']);
													return response()->json([
																	'code'=>400,
																	'message'=>'Gagal',
													]);
												}
								} catch (\Throwable $th) {
												Log::info('Gagal Update transaksi:'.$th->getMessage());
												DB::rollback();
												return response()->json([
													'code'=>400,
													'message'=>'Gagal'
												]);
								}
								DB::commit();
								$client = new \GuzzleHttp\Client();
								// if (isset($callback['saldo_after_trx'])) {
								// 	if ($callback['saldo_after_trx'] < 300000) {
								// 		$response = $client->request('GET','https://api.telegram.org/bot1344707970:AAGJFTejdmDzHwyEcjoCwl_DwOdSMRbnKdA/sendMessage?chat_id=1252367618&text=Saldo LarakostPulsa kamu sisa Rp '.$callback['saldo_after_trx'].', Silahkan topup lagi.');
								// 	}
								// }
								
								$statuses = $status;
								$no_trxes = $callback['api_idtrx'];
								$code = $product->code;
								
								$url = $cek->url_agen;
								if (isset($callback['total'])) {
									if ($callback['total'] > $cek->price) {
										$price = (int)($callback['total']+$product->markup);
									}else {
										$price = (int)($cek->price+$product->markup);
									}
								}else {
									$price = (int)($cek->price+$product->markup);
								}
								if ($statuses == 4) {
									$snes = $callback['token'];
								}else{
									$snes = null;
								}
								$array = [
										'no_trx' => $no_trxes,
										'status' => $statuses,
										'code'=> $code,
										'price'=>$price,
										'sn' => $snes,
										'note'=>$note
								];
								$response = $client->post($url , [
												'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
												'body' => json_encode($array)
								]);
								return response()->json([
												'code'=>200,
												'message'=>'Berhasil'
								]);
				}
				$callback = json_decode($request->content,true);
				Log::info('API CALLBACK LEWAT: '.$_SERVER['REMOTE_ADDR']);
				Log::info('CALLBACK LEWAT: '.json_encode($callback));
		}
		public function daftarsekolah(Request $request){
			Log::info('IP WEBSEKOLAH = '.$_SERVER['REMOTE_ADDR'].',DOMAIN = '.$request->url);
			if ($_SERVER['REMOTE_ADDR'] == '103.253.212.120') {
				return response()->json([
					'code'=>200,
					'data'=>[
						'token'=>$this->token,
						'sccn'=>$request->url
					]
				]);
			}elseif ($_SERVER['REMOTE_ADDR'] == '103.253.212.203') {
				return response()->json([
					'code'=>200,
					'data'=>[
						'token'=>$this->token,
						'sccn'=>$request->url
					]
				]);
			}else {
				return response()->json([
					'code'=>400,
					'data'=>[
						'token'=>'http://ngasalajaloh.com',
						'sccn'=>'bjhgjhgyjj'
					]
				]);
			}
		}
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DataHarga;
use App\DataTransaksiPpob;
use App\BukuSaldo;
use App\BankSupplier;
use App\DepositAgen;
use App\KategoriPascabayar;
use App\ProductPascabayar;
use App\HistoryTransaksiPascabayar;
use Auth;
use Log;
use DB;
use App\Helpers\Req;

class UserController extends Controller
{
    public function __construct()
    {
								$this->middleware('agen');
								$this->supplier = config('ppob-config')['supplier'];
    }
    public function data_user(Request $request){
        return Auth::user()->name;
    }
    public function data_harga(Request $request){
        $datas = DataHarga::get();
        $dt = [];
        foreach ($datas as $key => $value) {
            $dt[] = [
                'provider'=>$value['provider'],
                'provider_sub'=>$value['provider_sub'],
                'operator'=>$value['operator'],
                'operator_sub'=>$value['operator_sub'],
                'code'=>$value['code'],
                'description'=>$value['description'],
                'price'=>$value['price']+$value['markup'],
                'status'=>$value['status'],
            ];
        }
        return response()->json([
            'code'=>200,
            'message'=>'Data Harga Product',
            'data'=>$dt
        ]);
    }

				//cek tagihan
				public function kategori_pascabayar(Request $request){
						$datas = KategoriPascabayar::where('status',1)->orderBy('created_at','ASC')->get();
						return response()->json([
							'code'=>200,
							'message'=>'Data Kategori',
							'data'=>$datas
						]);
				}
				public function pembayaran_product(Request $request){
					// $data = [
					// 	'product_id' => $request['product_id'] // Kode Kategori (PLN, BPJS, TELKOM, dll)
					// ];
					// $result = Req::postLara($data,$this->supplier = 'lara', $add_url = 'pembayaran/product')->get();
					// $resul = ProductPascabayar::where('kategori_pascabayar_id',$request['product_id'])->where('status',1)->get();
					// $json = [
					// 	'status'=>'success',
					// 	'data'=>$resul
					// ];
					// $result = json_decode($json, TRUE);
					// if ($result['status'] == 'success') {
						// foreach ($result['data'] as $key => $value) {
						// 	$data = ProductPascabayar::updateOrCreate([
						// 		'id'=>$value['product_id'],
						// 		'kategori_pascabayar_id'=>$request['product_id']
						// 	],[
						// 		'product_name'=>$value['product_name'],
						// 		'fee'=>$value['fee_h2h'],
						// 		'status'=>$value['status']
						// 	]);
						// }
						$datas = ProductPascabayar::where('kategori_pascabayar_id',$request['product_id'])->where('status',1)->orderBy('id','ASC')->get();
						return response()->json([
							'code'=>200,
							'message'=>'Pembayaran Product',
							'data'=>$datas
						]);
					// }
					// return response()->json([
					// 	'code'=>400,
					// 	'message'=>$result['message'],
					// ]);
				}
				public function cek_tagihan(Request $request){
					date_default_timezone_set("Asia/Jakarta");
					if ((int)(date('Hi')) >= 2300 || (int)(date('Hi')) < 31) {
						return response()->json([
										'code'=>400,
										'message'=>'Maintenance jam 23:00 s/d jam 00:30 WIB',
										'data'=>'Maintenance jam 23:00 s/d jam 00:30 WIB'
						]);
					}
					$data = [
						'produk' => $request->produk, // Kode Produk (PLN, BPJSKS, TELKOM, SPEEDY)
						'nomor_rekening' => $request->id_pelanggan, // Nomor Pelanggan yang akan di bayarkan (No Meter Dll)
					];
					DB::beginTransaction();
					try {
						$result = Req::postLara($data,$this->supplier = 'larakostpulsa', $add_url = 'pembayaran/cektagihan')->get();
						Log::info('RESPON cek_tagihan :'.json_encode($result));
						if($result['status']=='success'){
											$fee_agen = (int)($result['fee']-((int)$result['fee']*20/100));
											$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
											$details = $result['data']['detail'];
											$detail = explode('|',$details);
											HistoryTransaksiPascabayar::create([
															'inquiry_id'=>$no_trx,
															'user_id'=>$request->user()->id,
															'fee'=>$result['fee'],
															'jml_bayar'=>$result['jml_bayar'],
															'fee_agen'=>$fee_agen,
															'jml_bayar_agen'=>(int)($result['data']['jumlah_bayar']-$fee_agen),
															'tagihan_id'=>$result['data']['tagihan_id'],
															'code'=>$result['data']['code'],
															'kategori'=>$detail[0],
															'no_pelanggan'=>$result['data']['no_pelanggan'],
															'periode'=>$result['data']['periode'],
															'nama'=>$result['data']['nama'],
															'jumlah_bulan'=>$result['data']['jumlah_bulan'],
															'jumlah_tagihan'=>$result['data']['jumlah_tagihan'],
															'admin'=>$result['data']['admin'],
															'jumlah_bayar'=>$result['data']['jumlah_bayar'],
															'detail'=>$result['data']['detail'],
															'status'=>1,
															'aktif'=>'no'
											]);
							}else{
											DB::rollback();
												$errors = $result['message'];
												$detail = explode(' ',$errors);
												if ($detail[1] == "[88]"){
																return response()->json([
																				'code'=>400,
																				'message'=>'Tagihan Sudah Dibayar'
																]);
												}elseif ($detail[1] == "[10]" || $detail[1] == "[14]"){
																return response()->json([
																				'code'=>400,
																				'message'=>'ID Pelanggan Salah'
																]);
												}
											$meg = explode(']',$errors);
											if (isset($meg[1])) {
												return response()->json([
															'code'=>400,
															'message'=>$meg[1]
											]);
											}
											return response()->json([
															'code'=>400,
															'message'=>$errors
											]);
							}

					} catch (\Throwable $th) {
						DB::rollback();
						Log::info('Inquiry Gagal Server:'.$th->getMessage());
						return response()->json([
										'code'=>400,
										'message'=>'Inquiry Gagal Server',
						]);
					}
					if ($result['status'] == 'success') {
						DB::commit();
						$data = HistoryTransaksiPascabayar::where('inquiry_id',$no_trx)->first();
						return response()->json([
							'code'=>200,
							'message'=>'Cek Tagihan',
							'fee'=>$data->fee_agen,
							'jml_bayar'=>(int)($data->jml_bayar_agen),
							'data'=>[
									'tagihan_id'=>$data['tagihan_id'],
									'code'=>$data['code'],
									'no_pelanggan'=>$data['no_pelanggan'],
									'periode'=>$data['periode'],
									'nama'=>$data['nama'],
									'jumlah_bulan'=>$data['jumlah_bulan'],
									'jumlah_tagihan'=>$data['jumlah_tagihan'],
									'admin'=>$data['admin'],
									'jumlah_bayar'=>$data['jumlah_bayar'],
									'detail'=>$data['detail']
							]
						]);
					}
					DB::rollback();
					return response()->json([
						'code'=>400,
						'message'=>$result['message'],
					]);
				}
				public function bayar_tagihan(Request $request){
					$tagihan_id = $request->tagihan_id;
					Log::info('MASUK PROSES TAGIHAN :'.$tagihan_id);
					DB::beginTransaction();
					try {
							$data = array(
								'tagihan_id' => $request->tagihan_id, // ID Tagihan
							);
							date_default_timezone_set("Asia/Jakarta");
							if ((int)(date('Hi')) >= 2300) {
								return response()->json([
												'code'=>400,
												'message'=>'Maintenance jam 23:00 s/d jam 00:00 WIB',
												'data'=>[
													'message'=>'Maintenance jam 23:00 s/d jam 00:00 WIB'
													]
								]);
							}
							Log::info('TIME ZONE PROSES TAGIHAN :'.$tagihan_id);
							$saldo = (float)$request->user()->saldo;
						$cek = HistoryTransaksiPascabayar::where('tagihan_id',$request->tagihan_id)->where('aktif','no')->first();
						if ($saldo < $cek->jml_bayar_agen) {
										return response()->json([
														'code'=>400,
														'message'=>'Saldo Supplier Kurang',
														'data'=>[
															'message'=>'Saldo Supplier Kurang'
															]
										]);
						}
						Log::info('SALDO CUKUP PROSES TAGIHAN :'.$tagihan_id);
						$debit = (int)$cek->jml_bayar_agen;
						$saldo_akhir = $saldo - $debit;

						$cek->aktif = 'yes';
						$cek->update();
						BukuSaldo::create([
										'user_id'=>$request->user()->id,
										'no_trx'=>$cek->inquiry_id,
										'mutasi'=>'Debet',
										'nominal'=>$debit,
										'saldo_akhir'=>$saldo_akhir,
										'keterangan'=>'Pembayaran tagihan'.$cek['kategori'].'('.$cek['543400482431'].')'
						]);
						$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
										'saldo'=>$saldo_akhir
						]);
						if (!$update_saldo) {
							DB::rollback();
							Log::info('GAGAL UPDATE SALDO PROSES TAGIHAN :'.$cek->inquiry_id);
							return response()->json([
											'code'=>400,
											'message'=>'Gagal proses pembayaran tagihan',
											'data'=>[
												'message'=>'Gagal proses pembayaran tagihan'
											]
							]);
						}
						Log::info('TEMBAK API PROSES TAGIHAN :'.$tagihan_id);
						$data = array(
							'tagihan_id' => $request->tagihan_id, // ID Tagihan
						);
						$result = Req::postLara($data,$this->supplier = 'larakostpulsa', $add_url = 'pembayaran/bayartagihan')->get();
						Log::info('RESPON PEMBAYARAN TAGIHAN :'.json_encode($result));
					} catch (\Throwable $th) {
						DB::rollback();
							Log::info('GAGAL UPDATE SALDO PROSES TAGIHAN :'.$th->getMessage());
							return response()->json([
											'code'=>400,
											'message'=>'Gagal proses pembayaran tagihan',
											'data'=>[
												'message'=>'Gagal proses pembayaran tagihan'
											]
							]);
					}

					if ($result['status']=='success') {
						DB::commit();
						return response()->json([
							'code'=>200,
							'message'=>'Pembayaran Berhasil',
							'data'=>$result
						]);
					}
					DB::rollback();
					return response()->json([
						'code'=>400,
						'message'=>'Pembayaran Gagal',
						'data'=>$result
					]);
					
				}

}

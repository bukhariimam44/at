<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DataHarga;
use App\DataTransaksiPpob;
use App\BukuSaldo;
use App\BankSupplier;
use App\DepositAgen;
use Auth;
use Log;
use DB;
use App\Helpers\Req;

class CekStatusController extends Controller
{
		public function __construct()
		{
						$this->middleware('agen');
						$this->supplier = config('ppob-config')['supplier'];
		}
		public function cek_status_ppob(Request $request){
			$ceks = DataTransaksiPpob::where('trxid_api',$request->trxid_api)->first();
			$ceks->on_check = 1;
			$ceks->update();
			$cek = DataTransaksiPpob::where('trxid_api',$request->trxid_api)->where('on_check','=',1)->first();
			$this->supplier = $cek->supplier;
			if ($this->supplier == 'javah2h' || $this->supplier == 'portalpulsa') {
				if ($cek->status_ppob_id == $request->status) {
						$data = array(
							'inquiry' => 'STATUS', // konstan
							'trxid_api' => $request->trxid_api, // Trxid atau Reffid dari sisi client saat transaksi pengisian
						);
						$response = Req::post($data,$this->supplier)->get();
						if ($response['result'] == 'success') {
							if ($response['message'][0]['status'] == '2' || $response['message'][0]['status'] == '3') {
								DB::beginTransaction();
								try {
									$saldo = $request->user()->saldo;
									$saldo_akhir = (int)$saldo + $cek->jual;
									// $saldos->saldo = $saldo_akhir;
									// $saldos->update();
									$bukus = new BukuSaldo;
									$bukus->user_id = $cek->user_id;
									$bukus->no_trx = date('ymdHis').$cek->user_id;
									$bukus->mutasi = 'Kredit';
									$bukus->nominal = $cek->jual;
									$bukus->saldo_akhir = $saldo_akhir;
									$bukus->keterangan = 'Gagal Transaksi '.$cek->no_trx;
									$bukus->save();
									$update = DataTransaksiPpob::find($cek->id);
									$update->status_ppob_id = $response['message'][0]['status'];
									$update->note = $response['message'][0]['note'];
									$update->update();
									
									$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
													'saldo'=>$saldo_akhir
									]);
									if (!$update_saldo) {
										DB::rollback();
										Log::info('GAGAL UPDATE CEK STATUS JAVA:');
										return response()->json([
														'code'=>401,
														'message'=>'Silahkan Diulangi kembali',
										]);
									}
								} catch (\Throwable $th) {
									Log::info('Gagal Update transaksi:'.$th->getMessage());
									DB::rollback();
									return response()->json([
										'code'=>400,
										'message'=>'Gagal input cek status 2 || 3',
									]);
								}
								DB::commit();
								return response()->json([
									'code'=>200,
									'message'=>'Cek status',
									'data'=>[
										'status'=>$response['message'][0]['status'],
										'sn'=>'',
										'ket'=>$response['message'][0]['note'],
									]
								]);
							}elseif ($response['message'][0]['status'] == '4') {
								$update = DataTransaksiPpob::find($cek->id);
								$update->status_ppob_id = $response['message'][0]['status'];
								$update->sn = $response['message'][0]['sn'];
								$update->price = $response['message'][0]['price'];
								$update->note = $response['message'][0]['note'];
								$update->update();
								return response()->json([
									'code'=>200,
									'message'=>'Cek status',
									'data'=>[
										'status'=>$response['message'][0]['status'],
										'sn'=>$response['message'][0]['sn'],
										'ket'=>$response['message'][0]['note'],
									]
								]);
							}
							
						}else {
							return response()->json([
								'code'=>400,
								'message'=>$response['message'],
							]);
						}
				}else {
					return response()->json([
						'code'=>200,
						'message'=>'Cek status',
						'data'=>[
							'status'=>$cek->status_ppob_id,
							'sn'=>$cek->sn,
							'ket'=>$cek->note
						]
					]);
				}
			}else {
				if ($cek->status_ppob_id == $request->status) {
					$data = array(
						'trxid' => $request->trxid_api, // Masukkan ID Transaksi Client
						);
						$response = Req::postLara($data,$this->supplier, $add_url = 'transaksi/history/detail')->get();
						if ($response['status'] == 'success') {
							if ($response['data']['status'] == '2' || $response['data']['status'] == '3') {
								DB::beginTransaction();
								try {
									$saldo = $request->user()->saldo;
									$saldo_akhir = (int)$saldo + $cek->jual;
									// $saldos->saldo = $saldo_akhir;
									// $saldos->update();
									
									$bukus = new BukuSaldo;
									$bukus->user_id = $cek->user_id;
									$bukus->no_trx = date('ymdHis').$cek->user_id;
									$bukus->mutasi = 'Kredit';
									$bukus->nominal = $cek->jual;
									$bukus->saldo_akhir = $saldo_akhir;
									$bukus->keterangan = 'Gagal Transaksi '.$cek->no_trx;
									$bukus->save();
									$update = DataTransaksiPpob::find($cek->id);
									$update->status_ppob_id = $response['data']['status'];
									$update->note = $response['data']['note'];
									$update->update();
									$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
													'saldo'=>$saldo_akhir
									]);
									if (!$update_saldo) {
										DB::rollback();
										Log::info('GAGAL UPDATE CEK STATUS LARA:');
										return response()->json([
														'code'=>401,
														'message'=>'Silahkan Diulangi kembali',
										]);
									}
								} catch (\Throwable $th) {
									Log::info('Gagal Update transaksi:'.$th->getMessage());
									DB::rollback();
									return response()->json([
										'code'=>400,
										'message'=>'Gagal input cek status 2 || 3',
									]);
								}
								DB::commit();
								return response()->json([
									'code'=>200,
									'message'=>'Cek status',
									'data'=>[
										'status'=>$response['data']['status'],
										'sn'=>'',
										'ket'=>$response['data']['note'],
									]
								]);
							}elseif ($response['data']['status'] == '1') {
								$update = DataTransaksiPpob::find($cek->id);
								$update->status_ppob_id = 4;
								$update->sn = $response['data']['token'];
								$update->note = $response['data']['note'];
								$update->price = $response['data']['total'];
								$update->update();
								return response()->json([
									'code'=>200,
									'message'=>'Cek status',
									'data'=>[
										'status'=>4,
										'sn'=>$response['data']['token'],
										'ket'=>$response['data']['note'],
									]
								]);
							}
							
						}else {
							return response()->json([
								'code'=>400,
								'message'=>$response['message'],
							]);
						}
				}else {
					return response()->json([
						'code'=>200,
						'message'=>'Cek status',
						'data'=>[
							'status'=>$cek->status_ppob_id,
							'sn'=>$cek->sn,
							'ket'=>$cek->note,
						]
					]);
				}
			}
			return response()->json([
				'code'=>400,
				'message'=>'Gagal Cek status',
			]);
		}
}

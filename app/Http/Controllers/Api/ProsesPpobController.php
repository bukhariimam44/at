<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DataHarga;
use App\DataTransaksiPpob;
use App\BukuSaldo;
use App\BankSupplier;
use App\DepositAgen;
use Auth;
use Log;
use DB;
use App\Helpers\Req;

class ProsesPpobController extends Controller
{
				public function __construct()
    {
								$this->middleware('agen');
								$this->supplier = config('ppob-config')['supplier'];
    }
				public function proses_pulsa(Request $request){
					date_default_timezone_set("Asia/Jakarta");
					if ((int)(date('Hi')) >= 2300 || (int)(date('Hi')) < 31) {
						return response()->json([
										'code'=>400,
										'message'=>'Maintenance jam 23:00 s/d jam 00:30 WIB',
										'data'=>'Maintenance jam 23:00 s/d jam 00:30 WIB'
						]);
					}
					if (!$product = DataHarga::where('code',$request['code'])->first()) {
						return response()->json([
										'code'=>400,
										'message'=>'Gagal',
										'data'=>'Produck Tidak Terdaftar'
						]);
					}
					if ($product->status == 'gangguan') {
						return response()->json([
										'code'=>400,
										'message'=>$product->description.' Sedang Gangguan',
										'data'=>$product->description.' Sedang Gangguan'
						]);
					}
					$this->supplier = $product->supplier;
					$jual = (int)$product->price + $product->markup;
					if ($request->user()->saldo < $jual) {
									return response()->json([
													'code'=>400,
													'message'=>'Saldo Supplier Kurang',
													'data'=>'Saldo Supplier Kurang'
									]);
					}
					$trxid_api = $request->no_trx;
					$cari = DataTransaksiPpob::where('tgl_trx',date('Y-m-d'))->where('code',$request['code'])->where('phone',$request['no_hp'])->get();
					$sequence = (int)count($cari)+1;
					$saldo_akhir = $request->user()->saldo - $jual;
					DB::beginTransaction();
					try {
									$add = DataTransaksiPpob::create([
													'user_id'=>$request->user()->id,
													'no_trx'=>$trxid_api,
													'tgl_trx'=>date('Y-m-d'),
													'code'=>$request['code'],
													'trxid_api'=>$trxid_api,
													'url_agen'=>$request->url_agen,
													'supplier'=>$this->supplier,
													'idcust'=>null,
													'phone'=>$request['no_hp'],
													'sequence'=>$sequence,
													'status_ppob_id'=>1,
													'jual'=>$jual,
													'price'=>$product->price,
													'last_balance'=>$saldo_akhir
									]);
									BukuSaldo::create([
													'user_id'=>$request->user()->id,
													'no_trx'=>$trxid_api,
													'mutasi'=>'Debet',
													'nominal'=>$jual,
													'saldo_akhir'=>$saldo_akhir,
													'keterangan'=>'Transaksi '.$product['description']
									]);
									$saldo = $request->user()->saldo;
									$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
													'saldo'=>$saldo_akhir
									]);
									if (!$update_saldo) {
										DB::rollback();
										Log::info('GAGAL UPDATE SALDO PROSES PULSA :'.$request['no_hp']);
										return response()->json([
														'code'=>400,
														'message'=>'Gagal proses Pulsa',
										]);
									}
									
									if ($this->supplier == 'javah2h' || $this->supplier == 'portalpulsa') {
										$data = array(
														'inquiry' => 'I', // konstan
														'code' => $request['code'], // kode produk
														'phone' => $request['no_hp'], // nohp pembeli
														'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
														'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 1,2,3,4,dst
										);
										$result = Req::post($data,$this->supplier)->get();
									}else {
										$data = 
												[
														'code' => $product->code_lara, // Kode Produk
														'target' => $request['no_hp'], // Nomor Handphone / ID Pelanggan
														'api_idtrx' => $trxid_api, // Trxid / Reffid dari sisi client
												];
											$result = Req::postLara($data,$this->supplier, $add_url = 'pembelian/transaction')->get();
										
									}
									
									
					} catch (\Throwable $th) {
									Log::info('Gagal proses Transaksi:'.$th->getMessage());
									DB::rollback();
									return response()->json([
													'code'=>400,
													'message'=>'Gagal proses pulsa',
													'data'=>$th
									]);
					}
					Log::info('RESPON POSES PPOB:'.json_encode($result));
					if (isset($result['result'])) {
						if ($result['result'] == 'success') {
										DB::commit();
										return response()->json([
														'code'=>200,
														'message'=>'Berhasil proses pulsa',
														'data'=>$result['message']
										]);
						}
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses pulsa',
										'data'=>$result['message']
						]);
					}else {
						if ($result['status'] == 'success') {
										DB::commit();
										return response()->json([
														'code'=>200,
														'message'=>'Berhasil proses pulsa',
														'data'=>$result['message']
										]);
						}
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses pulsa',
										'data'=>$result['message']
						]);
					}
					
			}
			public function proses_pln(Request $request){
				date_default_timezone_set("Asia/Jakarta");
					if ((int)(date('Hi')) >= 2300 || (int)(date('Hi')) < 31) {
						return response()->json([
										'code'=>400,
										'message'=>'Maintenance jam 23:00 s/d jam 00:30 WIB',
										'data'=>'Maintenance jam 23:00 s/d jam 00:30 WIB'
						]);
					}
					if (!$product = DataHarga::where('code',$request['code'])->first()) {
						return response()->json([
										'code'=>400,
										'message'=>'Gagal',
										'data'=>'Produck Tidak Terdaftar'
						]);
					}
					if ($product->status == 'gangguan') {
						return response()->json([
										'code'=>400,
										'message'=>$product->description.' Sedang Gangguan',
										'data'=>$product->description.' Sedang Gangguan'
						]);
					}
				$this->supplier = $product->supplier;
				$jual = (int)$product->price + $product->markup;
				if ($request->user()->saldo < $jual) {
								return response()->json([
												'code'=>400,
												'message'=>'Saldo Supplier Kurang',
												'data'=>'Saldo Supplier Kurang'
								]);
				}
				$trxid_api = $request->no_trx;
				$cari = DataTransaksiPpob::where('tgl_trx',date('Y-m-d'))->where('code',$request['code'])->where('phone',$request['no_meteran'])->get();
				$sequence = (int)count($cari)+1;
				$saldo_akhir = $request->user()->saldo - $jual;
				DB::beginTransaction();
				try {
					
								$add = DataTransaksiPpob::create([
												'user_id'=>$request->user()->id,
												'no_trx'=>$trxid_api,
												'tgl_trx'=>date('Y-m-d'),
												'code'=>$request['code'],
												'trxid_api'=>$trxid_api,
												'url_agen'=>$request->url_agen,
												'supplier'=>$this->supplier,
												'idcust'=>$request->no_meteran,
												'phone'=>$request->no_hp,
												'sequence'=>$sequence,
												'status_ppob_id'=>1,
												'jual'=>$jual,
												'price'=>$product->price,
												'last_balance'=>$saldo_akhir
								]);
								BukuSaldo::create([
												'user_id'=>$request->user()->id,
												'no_trx'=>$trxid_api,
												'mutasi'=>'Debet',
												'nominal'=>$jual,
												'saldo_akhir'=>$saldo_akhir,
												'keterangan'=>'Transaksi '.$product['description']
								]);
								$saldo = $request->user()->saldo;
								$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
												'saldo'=>$saldo_akhir
								]);
								if (!$update_saldo) {
									DB::rollback();
									Log::info('GAGAL UPDATE SALDO PROSES PLN :'.$request['no_meteran']);
									return response()->json([
													'code'=>400,
													'message'=>'Gagal proses PLN',
									]);
								}	
								if ($this->supplier == 'javah2h' || $this->supplier == 'portalpulsa') {
									$data = array(
										'inquiry' => 'PLN', // konstan
										'code' => $request['code'], // kode produk
										'phone' => $request['no_hp'], // nohp pembeli
										'idcust' => $request['no_meteran'], // nomor meter atau id pln
										'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
										'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 2,3,4,dst
										);
										$result = Req::post($data,$this->supplier)->get();		
								}else {
									$data = 
											[
													'code' => $product->code_lara, // Kode Produk
													'target' => $request['no_meteran'], // Nomor Handphone / ID Pelanggan
													'api_idtrx' => $trxid_api, // Trxid / Reffid dari sisi client
											];
										$result = Req::postLara($data,$this->supplier, $add_url = 'pembelian/transaction')->get();
									
								}			
								// $this->supplier = 'lara';
								// if ($this->supplier == 'lara') {
								// 	$data = 
								// 		[
								// 						'code' => $request['code'], // Kode Produk
								// 						'target' => $request['no_meteran'], // Nomor Handphone / ID Pelanggan
								// 						'api_idtrx' => $trxid_api, // Trxid / Reffid dari sisi client
								// 		];
								// 		$result = Req::postLara($data,$this->supplier = 'lara', $add_url = 'pembelian/transaction')->get();
								// }else {
								// 	$data = array(
								// 		'inquiry' => 'PLN', // konstan
								// 		'code' => $request['code'], // kode produk
								// 		'phone' => $request['no_hp'], // nohp pembeli
								// 		'idcust' => $request['no_meteran'], // nomor meter atau id pln
								// 		'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
								// 		'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 2,3,4,dst
								// 		);
								// 		$result = Req::post($data,$this->supplier)->get();		
								// }
										
				} catch (\Throwable $th) {
								Log::info('Gagal proses Transaksi PLN:'.$th->getMessage());
								DB::rollback();
								return response()->json([
												'code'=>400,
												'message'=>'Gagal proses pln',
												'data'=>$th
								]);
				}
				Log::info('RESPON POSES PLN:'.json_encode($result));
				if (isset($result['result'])) {
					if ($result['result'] == 'success') {
										DB::commit();
										return response()->json([
														'code'=>200,
														'message'=>'Berhasil proses PLN',
														'data'=>$result['message']
										]);
						}
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses PLN',
										'data'=>$result['message']
						]);
				}else {
					if ($result['status'] == 'success') {
									DB::commit();
									return response()->json([
													'code'=>200,
													'message'=>'Berhasil proses PLN',
													'data'=>$result['message']
									]);
					}
					DB::rollback();
					return response()->json([
									'code'=>400,
									'message'=>'Gagal proses PLN',
									'data'=>$result['message']
					]);
				}
				
		}
		public function proses_game(Request $request){
			date_default_timezone_set("Asia/Jakarta");
					if ((int)(date('Hi')) >= 2300 || (int)(date('Hi')) < 31) {
						return response()->json([
										'code'=>400,
										'message'=>'Maintenance jam 23:00 s/d jam 00:30 WIB',
										'data'=>'Maintenance jam 23:00 s/d jam 00:30 WIB'
						]);
					}
					if (!$product = DataHarga::where('code',$request['code'])->first()) {
						return response()->json([
										'code'=>400,
										'message'=>'Gagal',
										'data'=>'Produck Tidak Terdaftar'
						]);
					}

					if ($product->status == 'gangguan') {
						return response()->json([
										'code'=>400,
										'message'=>$product->description.' Sedang Gangguan',
										'data'=>$product->description.' Sedang Gangguan'
						]);
					}

			$this->supplier = $product->supplier;
			$jual = (int)$product->price + $product->markup;
			if ($request->user()->saldo < $jual) {
							return response()->json([
											'code'=>400,
											'message'=>'Saldo Supplier Kurang',
											'data'=>'Saldo Supplier Kurang'
							]);
			}
			$trxid_api = $request->no_trx;
			$cari = DataTransaksiPpob::where('tgl_trx',date('Y-m-d'))->where('code',$request['code'])->where('phone',$request['no_hp'])->get();
			$sequence = (int)count($cari)+1;
			$saldo_akhir = $request->user()->saldo - $jual;
			DB::beginTransaction();
			try {
							$add = DataTransaksiPpob::create([
											'user_id'=>$request->user()->id,
											'no_trx'=>$trxid_api,
											'tgl_trx'=>date('Y-m-d'),
											'code'=>$request['code'],
											'trxid_api'=>$trxid_api,
											'url_agen'=>$request->url_agen,
											'supplier'=>$this->supplier,
											'idcust'=>$request['idcust'],
											'phone'=>$request['no_hp'],
											'sequence'=>$sequence,
											'status_ppob_id'=>1,
											'jual'=>$jual,
											'price'=>$product->price,
											'last_balance'=>$saldo_akhir
							]);
							BukuSaldo::create([
											'user_id'=>$request->user()->id,
											'no_trx'=>$trxid_api,
											'mutasi'=>'Debet',
											'nominal'=>$jual,
											'saldo_akhir'=>$saldo_akhir,
											'keterangan'=>'Transaksi '.$product['description']
							]);
							$saldo = $request->user()->saldo;
							$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
											'saldo'=>$saldo_akhir
							]);
							if (!$update_saldo) {
								DB::rollback();
								Log::info('GAGAL UPDATE SALDO PROSES GAME :'.$request['idcust']);
								return response()->json([
												'code'=>400,
												'message'=>'Gagal proses GAME',
								]);
							}
							if ($this->supplier  == 'javah2h' || $this->supplier == 'portalpulsa') {
								$data = array(
												'inquiry' => 'I', // konstan
												'code' => $request['code'], // kode produk
												'phone' => $request['no_hp'], // nohp pembeli
												'idcust' => $request['idcust'], // nohp pembeli
												'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
												'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 1,2,3,4,dst
								);
								$result = Req::post($data,$this->supplier)->get();
							}else {
								$data = 
										[
												'code' => $product->code_lara, // Kode Produk
												'target' => $request['idcust'], // Nomor Handphone / ID Pelanggan
												'api_idtrx' => $trxid_api, // Trxid / Reffid dari sisi client
										];
									$result = Req::postLara($data,$this->supplier, $add_url = 'pembelian/transaction')->get();
								
							}
							
			} catch (\Throwable $th) {
							Log::info('Gagal proses Transaksi GAME:'.$th->getMessage());
							DB::rollback();
							return response()->json([
											'code'=>400,
											'message'=>'Gagal proses game',
											'data'=>$th
							]);
			}
			Log::info('RESPON PROSES GAME:'.json_encode($result));
			if (isset($result['result'])) {
				if ($result['result'] == 'success') {
									DB::commit();
									return response()->json([
													'code'=>200,
													'message'=>'Berhasil proses game',
													'data'=>$result['message']
									]);
					}
					DB::rollback();
					return response()->json([
									'code'=>400,
									'message'=>$result['message'],
									'data'=>$result['message']
					]);
			}else {
				if ($result['status'] == 'success') {
									DB::commit();
									return response()->json([
													'code'=>200,
													'message'=>'Berhasil proses game',
													'data'=>$result['message']
									]);
					}
					DB::rollback();
					return response()->json([
									'code'=>400,
									'message'=>$result['message'],
									'data'=>$result['message']
					]);
			}
			
	}

	public function proses_e_money(Request $request){
		date_default_timezone_set("Asia/Jakarta");
					if ((int)(date('Hi')) >= 2300 || (int)(date('Hi')) < 31) {
						return response()->json([
										'code'=>400,
										'message'=>'Maintenance jam 23:00 s/d jam 00:30 WIB',
										'data'=>'Maintenance jam 23:00 s/d jam 00:30 WIB'
						]);
					}
					if (!$product = DataHarga::where('code',$request['code'])->first()) {
						return response()->json([
										'code'=>400,
										'message'=>'Gagal',
										'data'=>'Produck Tidak Terdaftar'
						]);
					}

					if ($product->status == 'gangguan') {
						return response()->json([
										'code'=>400,
										'message'=>$product->description.' Sedang Gangguan',
										'data'=>$product->description.' Sedang Gangguan'
						]);
					}

		$this->supplier = $product->supplier;
		$jual = (int)($product->price + $product->markup);
		if ($request->user()->saldo < $jual) {
						return response()->json([
										'code'=>400,
										'message'=>'Saldo Supplier Kurang',
										'data'=>'Saldo Supplier Kurang'
						]);
		}
		$trxid_api = $request->no_trx;
		$cari = DataTransaksiPpob::where('tgl_trx',date('Y-m-d'))->where('code',$request['code'])->where('phone',$request['no_hp'])->get();
		$sequence = (int)count($cari)+1;
		$saldo_akhir = $request->user()->saldo - $jual;
		DB::beginTransaction();
		try {
						$add = DataTransaksiPpob::create([
										'user_id'=>$request->user()->id,
										'no_trx'=>$trxid_api,
										'tgl_trx'=>date('Y-m-d'),
										'code'=>$request['code'],
										'trxid_api'=>$trxid_api,
										'url_agen'=>$request->url_agen,
										'supplier'=>$this->supplier,
										'idcust'=>null,
										'phone'=>$request['no_hp'],
										'sequence'=>$sequence,
										'status_ppob_id'=>1,
										'jual'=>$jual,
										'price'=>$product->price,
										'last_balance'=>$saldo_akhir
						]);
						BukuSaldo::create([
										'user_id'=>$request->user()->id,
										'no_trx'=>$trxid_api,
										'mutasi'=>'Debet',
										'nominal'=>$jual,
										'saldo_akhir'=>$saldo_akhir,
										'keterangan'=>'Transaksi '.$product['description']
						]);
						$saldo = $request->user()->saldo;
						$update_saldo = User::where('id',$request->user()->id)->where('saldo','=',$saldo)->update([
										'saldo'=>$saldo_akhir
						]);
						if (!$update_saldo) {
							DB::rollback();
							Log::info('GAGAL UPDATE SALDO PROSES E-Money :'.$request['no_hp']);
							return response()->json([
											'code'=>400,
											'message'=>'Gagal proses E-Money',
							]);
						}
						if ($this->supplier == 'javah2h' || $this->supplier == 'portalpulsa') {
							$data = array(
											'inquiry' => 'I', // konstan
											'code' => $request['code'], // kode produk
											'phone' => $request['no_hp'], // nohp pembeli
											'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
											'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 1,2,3,4,dst
							);
							$result = Req::post($data,$this->supplier)->get();
						}else {
							$data = 
									[
											'code' => $product->code_lara, // Kode Produk
											'target' => $request['no_hp'], // Nomor Handphone / ID Pelanggan
											'api_idtrx' => $trxid_api, // Trxid / Reffid dari sisi client
									];
								$result = Req::postLara($data,$this->supplier, $add_url = 'pembelian/transaction')->get();
							
						}
						// $data = array(
						// 				'inquiry' => 'I', // konstan
						// 				'code' => $request['code'], // kode produk
						// 				'phone' => $request['no_hp'], // nohp pembeli
						// 				'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
						// 				'no' => $sequence, // untuk isi lebih dari 1x dlm sehari, isi urutan 1,2,3,4,dst
						// );
						// $result = Req::post($data,$this->supplier)->get();
		} catch (\Throwable $th) {
						Log::info('Gagal proses E-Money:'.$th->getMessage());
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses E-Money',
										'data'=>$th
						]);
		}
		Log::info('RESPON PROSES E-Money:'.json_encode($result));
		if (isset($result['result'])) {
			if ($result['result'] == 'success') {
								DB::commit();
								return response()->json([
												'code'=>200,
												'message'=>'Berhasil proses E-Money',
												'data'=>$result['message']
								]);
				}
				DB::rollback();
				return response()->json([
								'code'=>400,
								'message'=>'Gagal proses E-Money',
								'data'=>$result['message']
				]);
		}else {
			if ($result['status'] == 'success') {
								DB::commit();
								return response()->json([
												'code'=>200,
												'message'=>'Berhasil proses E-Money',
												'data'=>$result['message']
								]);
				}
				DB::rollback();
				return response()->json([
								'code'=>400,
								'message'=>'Gagal proses E-Money',
								'data'=>$result['message']
				]);
		}
		
}
}

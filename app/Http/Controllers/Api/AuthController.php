<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Log;

class AuthController extends Controller
{
    public $successStatus = 200;
    public function login(Request $request){ 
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('AtApp')->accessToken; 
            $respon = [
                'code'=>200,
                'message'=>'Login berhasil',
                'data'=>[
                    'nama'=>$user->name,
                    'email'=>$user->email,
                    'username'=>$user->username,
                    'nohp'=>$user->nohp,
                    'type'=>$user->typeId->name,
                    'accessToken'=>$success['token']
                ]
            ];
            return response()->json($respon, 200); 
        }else{ 
            return response()->json([
                'code'=>'400',
                'message'=>'Login Gagal'
            ]); 
        } 
    }
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'nohp' => 'required',
            'username' => 'required',
            'password' => 'required', 
            'type'=>'required'
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('OpsApp')->accessToken; 
        $success['name'] =  $user->name;
        $input['token'] = $success['token'];
        
        return response()->json(['success'=>$success], $this->successStatus); 
    }
}

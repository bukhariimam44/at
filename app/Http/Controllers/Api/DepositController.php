<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DataHarga;
use App\DataTransaksiPpob;
use App\BukuSaldo;
use App\BankSupplier;
use App\DepositAgen;
use Auth;
use Log;
use DB;
use App\Helpers\Req;

class DepositController extends Controller
{
	public function __construct()
	{
					$this->middleware('agen');
					$this->supplier = config('ppob-config')['supplier'];
	}
	public function data_bank(Request $request){
		$bank = BankSupplier::where('status',1)->get();
		$transaksi = DepositAgen::where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
		return response()->json([
			'bank'=>$bank,
			'transaksi'=>$transaksi
		]);
	}
	public function proses_topup(Request $request){
		$bank = BankSupplier::find($request->bank);
		$nominal = $request->nominal;
		Log::info('Bank : '.$bank->nama.', Nominal : '.$nominal);
		//paslu
		// $unik = rand(100,999);
		// $total = (int)$request->nominal+$unik;
		// $response = [
		// 	'result'=>'success',
		// 	'message'=>'Silakan transfer sebesar Rp '.$total.',- Ke Rekening: BCA, no. 0770520207, a.n. BENY ARIF L. Batas waktu transfer 1x24jam'
		// ];
		//end palsu

		$data = array(
			'inquiry' => 'D', // konstan
			'bank' => $bank->nama, // bank tersedia: bca, bni, mandiri, bri, muamalat
			'nominal' => $nominal, // jumlah request
			);
		$response = Req::post($data,$this->supplier)->get();
		if ($response['result'] == 'success') {
			$exp = explode(" " , $response['message']);
			$pisah = $exp[4];
			$nom = explode("," , $pisah);
			$nominal = str_replace('.', '', $nom[0]);
			$dep = DepositAgen::create([
				'user_id'=>$request->user()->id,
				'no_trx'=>date('ymdhis').$request->user()->id,
				'tgl_trx'=>date('Y-m-d'),
				'nominal'=>$nominal,
				'bank_supplier_id'=>$bank->id,
				'status'=>'menunggu',
				'user_admin'=>$request->user()->id,
				'keterangan'=>$response['message']
			]);
			if ($dep) {
				return response()->json([
					'code'=>200,
					'message'=>'Deposit Berhasil berhasil',
					'data'=>$response['message']
				]);
			}
		}else {
			return response()->json([
				'code'=>400,
				'message'=>$response['message'],
			]);
		}
	}
	public function buku_saldo(Request $request){
		$buku = BukuSaldo::where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
		return response()->json([
			'code'=>200,
			'message'=>'Buku Saldo',
			'user'=> $request->user(),
			'data'=>$buku
		]);
	}
}

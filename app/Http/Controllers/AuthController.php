<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataHarga;
use Auth;
use DB;
use Log;
use App\Helpers\UpdateHarga;
use App\Helpers\Req;
use App\DataTransaksiPpob;
use App\DepositAgen;
use App\BukuSaldo;
use App\User;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function masuk(Request $request){
        if ($request->username && $request->password) {
            if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){ 
                flash('Selamat Datang '.$request->user()->name)->success();
                return redirect()->route('home');
            }
            flash('Username atau Password Salah.')->error();
            return view('auth.login');
        }
        return view('auth.login');
    }
    // public function call(){
				// 	$client = new \GuzzleHttp\Client();
				// 				$url = 'https://fixpay.kmbti.com/api/callback/server';
				// 				$array = [
				// 					'no_trx' => '2006270557471',
				// 						'status' => '4',
				// 						'sn' => '24235436346346'
				// 				];
				// 				$response = $client->post($url , [
				// 								'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
				// 								'body' => json_encode($array)
				// 				]);
				// }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Req;
use App\DataHarga;

class HargaController extends Controller
{
    public function __construct()
    {
								$this->middleware('admin');
								$this->supplier = config('ppob-config')['supplier'];
    }
    public function update_harga(Request $request){
        $data = array(
            'inquiry' => 'HARGA', // konstan
            'code' => $request->product, // pilihan: pln, pulsa, game
        );
        $result = Req::post($data,$this->supplier)->get();
        foreach ($result['message'] as $key => $value) {
            DataHarga::create([
                'provider'=>$value['provider'],
                'provider_sub'=>$value['provider_sub'],
                'operator'=>$value['operator'],
                'operator_sub'=>$value['operator_sub'],
                'code'=>$value['code'],
                'description'=>$value['description'],
                'price'=>$value['price'],
                'status'=>$value['status']
            ]);
        }
        return response()->json($result);
    }
}

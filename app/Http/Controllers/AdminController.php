<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataHarga;
use Auth;
use DB;
use Log;
use App\Helpers\UpdateHarga;
use App\Helpers\Req;
use App\DataTransaksiPpob;
use App\DepositAgen;
use App\BukuSaldo;
use App\BankSupplier;
use App\StatusPpob;
use App\User;
use GuzzleHttp\Client;

class AdminController extends Controller
{
		public function __construct()
		{
						$this->supplier = config('ppob-config')['supplier'];
						$this->hsl = '';	
		}
		public function home(){
					return view('home');
		}
		public function harga_product(Request $request,$id){
					if ($request->action == 'update') {
									DB::beginTransaction();

									$kode = $request->provider;
									$ids = $request->ids;
									$data = array(
													'inquiry' => 'HARGA', // konstan
													'code' => $kode, // pilihan: pln, pulsa, game
									);
									try {
												if ($kode == 'game') {
													$result = Req::post($data,'portalpulsa')->get();
													Log::info('RESPON UPDATE GAME portal :'.json_encode($result));
													foreach ($result['message'] as $key => $value) {
															if ($update = DataHarga::where('code',$value['code'])->where('supplier','=','portalpulsa')->first()) {
																$update->code = $value['code'];
																$update->description = $value['description'];
																if ($update->price >= $value['price']) {
																	$price_baru = $update->price;
																}else {
																	$price_baru = $value['price'];
																}
																$update->price = $price_baru;
																$update->status = $value['status'];
																$update->update();
															}
																	
													}

													$data = array(
														'product_id' => $kode, // Kode Operator (TSEL, ISAT, AXIS, dll)
														);
														$result = Req::postLara($data,$this->supplier, $add_url = 'pembelian/product')->get();
														foreach ($result['data'] as $key => $value) {
															if ($update = DataHarga::where('code_lara',$value['product_id'])->where('supplier','=','larakostpulsa')->first()) {
																$update->code_lara = $value['product_id'];
																$update->description = $value['product_name'];
																if ($update->price >= $value['h2h_price']) {
																	$price_baru = $update->price;
																}else {
																	$price_baru = $value['h2h_price'];
																}
																$update->price = $price_baru;
																if ($value['status'] == 1) {
																	$status = 'normal';
																}else {
																	$status = 'gangguan';
																}
																$update->status = $status;
																$update->update();
															}
														}
												}else {
													$result = Req::post($data,'javah2h')->get();
													Log::info('RESPON UPDATE GAME java :'.json_encode($result));
													foreach ($result['message'] as $key => $value) {
																	if ($update = DataHarga::where('code',$value['code'])->where('supplier','=','javah2h')->first()) {
																		$update->code = $value['code'];
																		$update->description = $value['description'];
																		if ($update->price >= $value['price']) {
																			$price_baru = $update->price;
																		}else {
																			$price_baru = $value['price'];
																		}
																		$update->price = $price_baru;
																		$update->status = $value['status'];
																		$update->update();
																	}
																	
													}


												}
													
													foreach ($request->ids as $key => $value) {
																	DataHarga::find($value)->update([
																					'markup'=>str_replace(",", "", $request->markup[$key])
																	]);
													}
									} catch (\Throwable $th) {
													Log::info('GAGAL :'.$th);
													flash('Gagal Update.')->error();
													$this->hsl = 'gagal';
									}
									if ($this->hsl == '') {
													DB::commit();
													flash('Berhasil Update')->success();
									}
									
					}
					if ($id == 'pulsa') {
									$product = 'Pulsa';
									$datas = DataHarga::where('provider_sub','REGULER')->get();
					}elseif ($id == 'pln') {
									$product = 'Token Listrik';
									$datas = DataHarga::where('provider_sub','PLN')->get();
					}elseif ($id == 'game') {
									$product = 'Voucher Game';
									$datas = DataHarga::where('provider','GAME')->get();
					}elseif ($id == 'paket-internet') {
									$product = 'Paket Internet';
									$datas = DataHarga::where('provider_sub','INTERNET')->get();
					}elseif ($id == 'paket-sms') {
									$product = 'Paket SMS';
									$datas = DataHarga::where('provider_sub','SMS')->get();
					}elseif ($id == 'paket-telpon') {
									$product = 'Paket Telpon';
									$datas = DataHarga::where('provider_sub','TELPON')->get();
					}elseif ($id == 'saldo-OVO') {
									$product = 'Saldo OVO';
									$datas = DataHarga::where('provider','OVO')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Gojek') {
									$product = 'Saldo Gojek';
									$datas = DataHarga::where('provider','GOJEK')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Gojek-Driver') {
									$product = 'Saldo Gojek Driver';
									$datas = DataHarga::where('provider','GOJEK DRIVER')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Grab') {
									$product = 'Saldo Grab';
									$datas = DataHarga::where('provider','GRAB')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Grab-Driver') {
									$product = 'Saldo Grab Driver';
									$datas = DataHarga::where('provider','GRAB DRIVER')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Linkaja') {
									$product = 'Saldo LinkAja';
									$datas = DataHarga::where('provider','LINKAJA')->orderBy('price','ASC')->get();
					}elseif ($id == 'saldo-Shopeepay') {
									$product = 'Saldo Shopeepay';
									$datas = DataHarga::where('provider','SHOPEEPAY')->orderBy('price','ASC')->get();
					}elseif ($id == 'wifi-id') {
									$product = 'Wifi ID';
									$datas = DataHarga::where('provider','SPEEDY')->orderBy('price','ASC')->get();
					}elseif ($id == 'google-play') {
									$product = 'Google Play';
									$datas = DataHarga::where('provider','GOOGLE PLAY')->orderBy('price','ASC')->get();
					}
					$ids = $id;
					if ($id == 'game') {
									$code = 'game';
					}elseif ($id == 'pln') {
									$code = 'pln';
					}else {
									$code = 'pulsa';
					}
					return view('harga_product',compact('datas','product','ids','code'));
		}
		public function logout(){
					Auth::guard()->logout();
					return redirect()->back();
		}

		public function request_topup(Request $request){
		$datas = DepositAgen::orderBy('created_at','DESC')->get();
		return view('request_topup',compact('datas'));
		}
		public function proses_topup(Request $request){
		$data = DepositAgen::find($request->ids);
		if ($data) {
			if ($data->status == 'menunggu') {
				DB::beginTransaction();
				try {
					$user = User::find($data->user_id);
					$saldo_akhir = (int)$user->saldo+$data->nominal;
					$user->saldo = $saldo_akhir;
					$user->update();
					$dep = DepositAgen::find($data->id);
					$dep->status = 'berhasil';
					$dep->update();
					BukuSaldo::create([
						'user_id'=>$data->user_id,
						'no_trx'=>$data->no_trx,
						'mutasi'=>'Kredit',
						'nominal'=>$data->nominal,
						'saldo_akhir'=>$saldo_akhir,
						'keterangan'=>'Penambahan Saldo '.$dep->bankId->nama
					]);
				} catch (\Throwable $th) {
					Log::info('Gagal topup deposit:'.$th->getMessage());
					DB::rollback();
					flash('Gagal Proses Deposit')->error();
					return redirect()->back();
				}
				DB::commit();
				flash('Proses Berhasil')->success();
				return redirect()->back();
			}else {
				flash('Gagal Sudah diproses')->error();
				return redirect()->back();
			}
		}
		flash('Gagal Proses Deposit')->error();
		return redirect()->back();
		}
		public function buku_saldo_agen(Request $request){
			$datas = [];
			$username = $request->username;
			$bulan = $request->bulan;
			$tahun = $request->tahun;
			if ($request->username && $request->bulan && $request->tahun) {
				$user = User::where('username',$request->username)->where('aktif',1)->first();
				if ($user) {
					$datas = BukuSaldo::where('user_id',$user->id)->where('created_at','LIKE','%'.$request->tahun.'-'.$request->bulan.'%')->orderBy('created_at','DESC')->get();
				}
				
			}
			return view('buku_saldo_agen',compact('datas','username','bulan','tahun'));
		}
		public function topup_manual(Request $request){
			if ($request->bank) {
				DB::beginTransaction();
				try {
					$user = User::where('username',$request->username)->first();
					$saldo_akhir = $user->saldo + $request->nominal;
					$user->saldo = $saldo_akhir;
					$user->update();
					$deposit = new DepositAgen;
					$deposit->user_id = $user->id;
					$deposit->no_trx = date('YmdHis').$user->id;
					$deposit->tgl_trx = date('Y-m-d');
					$deposit->nominal = $request->nominal;
					$deposit->bank_supplier_id = $request->bank;
					$deposit->status = 'berhasil';
					$deposit->keterangan = 'Topup Manual';
					$deposit->user_admin = $request->user()->id;
					$deposit->save();
					$buku = new BukuSaldo;
					$buku->user_id = $user->id;
					$buku->no_trx = date('YmdHis').$user->id;
					$buku->mutasi = 'Kredit';
					$buku->nominal = $request->nominal;
					$buku->saldo_akhir = $saldo_akhir;
					$buku->keterangan = 'Topup Manual';
					$buku->save();
				} catch (\Throwable $th) {
					Log::info('Gagal Topup Manual:'.$th->getMessage());
					DB::rollback();
					return redirect()->back();
				}
				DB::commit();
				flash('Topup Manual Berhasil')->success();
				return redirect()->back();
				
			}
			$banks =  BankSupplier::get();
			return view('topup_manual',compact('banks'));
		}
		public function update_harga_agen(Request $request){

					$client = new \GuzzleHttp\Client();
					$agen = User::where('username',$request->username)->first();
					$url = $agen->domain.'/api/update_harga/price';

					$array = [
							'action' => 'update_harga',
					];
					try {
						$response = $client->post($url , [
										'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
										'body' => json_encode($array)
						]);
					} catch (\Throwable $th) {
						return response()->json([
										'code'=>400,
										'message'=>'Gagal'
						]);
					}
					return response()->json([
									'code'=>200,
									'message'=>'Berhasil',
									'data'=>$response
					]);
		}
		public function data_transaksi_ppob(Request $request){
			if ($request->action == 'cari') {
				$from =$request->from;
				$to = $request->to;
			}else {
				$from =date('Y-m-d');
				$to = date('Y-m-d');
			}
			$user = $request->userid;
			$phone = $request->phone;
			$no_trx = $request->no_trx;
			$status = $request->status;
			$stts = $status;
			$supplier = $request->supplier;
			$datas = DataTransaksiPpob::where('user_id','LIKE','%'.$user.'%')->where('supplier','LIKE','%'.$supplier.'%')->whereBetween('tgl_trx',[$from,$to])->where('no_trx','LIKE','%'.$no_trx.'%')->where('phone','LIKE','%'.$phone.'%')->where('status_ppob_id','LIKE','%'.$status.'%')->orderBy('id','DESC')->get();
			$statuses = StatusPpob::get();
			return view('data_transaksi_ppob',compact('datas','from','to','statuses','stts','supplier'));
		}
		public function laporan_transaksi_ppob(Request $request){
			if ($request->action == 'cari') {
				$from =$request->from;
				$to = $request->to;
			}else {
				$from =date('Y-m-d');
				$to = date('Y-m-d');
			}
			$phone = $request->phone;
			$no_trx = $request->no_trx;
			$statuses = StatusPpob::get();
			$datas = DB::table('data_transaksi_ppobs')
			->join('users','data_transaksi_ppobs.user_id','=','users.id')
			->select('users.username as username',DB::raw('SUM(data_transaksi_ppobs.price) as price'),DB::raw('SUM(data_transaksi_ppobs.jual) as jual'))
			->whereBetWeen('data_transaksi_ppobs.tgl_trx',[$from,$to])
			->where('data_transaksi_ppobs.status_ppob_id',4)
			->groupBy('users.username')->get();
			// return $datas;
			return view('laporan_transaksi',compact('datas','from','to'));
		}
		public function cek_status(Request $request){
			DB::beginTransaction();
			try {
				$cari = DataTransaksiPpob::find($request->ids);
				if ($cari->supplier == 'larakostpulsa') {
						$this->supplier = 'larakostpulsa';
						$data = array(
							'trxid' => $cari->trxid_api, // Masukkan ID Transaksi Client
						);
						$result = Req::postLara($data,$this->supplier,$add_url = 'transaksi/history/detail')->get();
					Log::info('RESPON cek status PPOB:'.json_encode($result));
					if ($result['status'] == 'success') {
							$url = $cari->url_agen;
							$price = $cari->jual;
							if (isset($result['data']['token'])) {
								$snes = $result['data']['token'];
							}else{
								$snes = null;
							}
							if ($result['data']['status'] == 1) {
								$status = 4;
							}elseif ($result['data']['status'] == 2 || $result['data']['status'] == 3) {
								$status = 2;
							}else {
								$status = 1;
							}
							$array = [
								'no_trx' => $cari->trxid_api,
								'status' => $status,
								'code'=> $cari->code,
								'price'=>$price,
								'sn' => $snes
						];
						$client = new \GuzzleHttp\Client();
						if ($status == 4) {
										$cari->note = $result['data']['note'];
										$cari->price = $result['data']['total'];
										$response = $client->post($url , [
														'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
														'body' => json_encode($array)
										]);
						}elseif ($status == 2 || $status == 3) {
										$users = User::find($cari->user_id);
										$saldo_akhirs = (int)($users->saldo + $cari->jual);
										$users->saldo = $saldo_akhirs;
										$users->update();
										$cari->note = $result['data']['note'];
										BukuSaldo::create([
														'user_id'=>$cari->user_id,
														'no_trx'=>date('ymdhis').$cari->user_id.rand(100,999),
														'mutasi'=>'Kredit',
														'nominal'=>(int)$cari->jual,
														'saldo_akhir'=>(int)$saldo_akhirs,
														'keterangan'=>'Refund Gagal Transaksi '.$cari->trxid_api
										]);
										$response = $client->post($url , [
														'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
														'body' => json_encode($array)
										]);
						}
						$cari->status_ppob_id = $status;
						$cari->update();
					}else {
						DB::rollback();
						flash('Result Failed')->error();
						return redirect()->back();
					}
				}else{
					$this->supplier = $cari->supplier;
					$data = array(
						'inquiry' => 'STATUS', // konstan
						'trxid_api' => $cari->trxid_api, // Trxid atau Reffid dari sisi client saat transaksi pengisian
						);
						$result = Req::post($data,$this->supplier)->get();
					Log::info('RESPON cek status PPOB:'.json_encode($result));
					if ($result['result'] == 'success') {
							$url = $cari->url_agen;
							$price = $cari->jual;
							if (isset($result['message'][0]['sn'])) {
								$snes = $result['message'][0]['sn'];
							}else{
								$snes = null;
							}
							$array = [
								'no_trx' => $cari->trxid_api,
								'status' => $result['message'][0]['status'],
								'code'=> $cari->code,
								'price'=>$price,
								'sn' => $snes
						];
						$client = new \GuzzleHttp\Client();
						if ($result['message'][0]['status'] == 4) {
										$cari->note = $result['message'][0]['note'];
										$cari->price = $result['message'][0]['price'];
										$response = $client->post($url , [
														'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
														'body' => json_encode($array)
										]);
						}elseif ($result['message'][0]['status'] == 2 || $result['message'][0]['status'] == 3) {
										$users = User::find($cari->user_id);
										$saldo_akhirs = (int)($users->saldo + $cari->jual);
										$users->saldo = $saldo_akhirs;
										$users->update();
										$cari->note = $result['message'][0]['note'];
										BukuSaldo::create([
														'user_id'=>$cari->user_id,
														'no_trx'=>date('ymdhis').$cari->user_id.rand(100,999),
														'mutasi'=>'Kredit',
														'nominal'=>(int)$cari->jual,
														'saldo_akhir'=>(int)$saldo_akhirs,
														'keterangan'=>'Refund Gagal Transaksi '.$cari->trxid_api
										]);
										$response = $client->post($url , [
														'headers' => ['Accept' => 'application/json','Content-Type' => 'application/json'],
														'body' => json_encode($array)
										]);
						}
						$cari->status_ppob_id = $result['message'][0]['status'];
						$cari->update();
					}else {
						DB::rollback();
						flash('Result Failed')->error();
						return redirect()->back();
					}
				}
				
					
			} catch (\Throwable $th) {
				Log::info('Gagal cek status PPOB:'.$th->getMessage());
				DB::rollback();
				flash('Gagal cek status PPOB.')->error();
				return redirect()->back();
			}
			DB::commit();
			flash('Berhasil cek status PPOB.')->success();
			return redirect()->back();
		}
		public function request_transfer(Request $request){
			$datas = [];
			return view('request_transfer',compact('datas'));
		}
}

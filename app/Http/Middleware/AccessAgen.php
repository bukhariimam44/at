<?php

namespace App\Http\Middleware;

use Closure;

class AccessAgen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->aktif == 1 && $request->user()->type == 2) {
									if ($request->user()->saldo < 200000) {
										$client = new \GuzzleHttp\Client();
										$response = $client->request('GET','https://api.telegram.org/bot'.$request->user()->token_telegram.'/sendMessage?chat_id='.$request->user()->id_telegram.'&text=Saldo Supplier kamu sisa Rp '.$request->user()->saldo.', Silahkan topup kembali.');
									}
									return $next($request);
        }
        return response()->json([
            'code'=>401,
            'message'=>'Tidak boleh akses agen'
        ]);
    }
}

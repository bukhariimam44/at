<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositAgen extends Model
{
	protected $fillable = [
		'id','user_id', 'no_trx','tgl_trx','nominal','bank_supplier_id','status','user_admin'	,'keterangan'
	];
	public function bankId(){
		return $this->belongsTo('App\BankSupplier','bank_supplier_id');
	}
}

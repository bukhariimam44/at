<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div> 
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-color-2 sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5">
                <!-- <li>
                  <a class="text-white" href="#">FAQ</a>
                </li>
                <li class="text-white">|</li> -->
                @if(Auth::guard()->check())
                <li>
                  <a class="text-white" href="#">{{Auth::guard()->user()->name}}</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="{{url('/logout')}}">Keluar</a>
                </li>
                @else
                <!-- <li>
                  <a class="text-white" href="">Daftar</a>
                </li>
                <li class="text-white">|</li> -->
                <li>
                  <a class="text-white" href="{{route('login')}}">Login</a>
                </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="widget m-0 pull-right sm-pull-none sm-text-center">
              <ul class="list-inline pull-right">
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-cart-link has-dropdown text-white text-hover-theme-colored">
                    <!-- <i class="fa fa-shopping-cart font-13"></i> (2) -->
                    </a>
                    <ul class="dropdown">
                      <li>
                        <!-- dropdown cart -->
                        <div class="dropdown-cart">
                          <table class="table cart-table-list table-responsive">
                            <tbody>
                              <tr>
                                <td><a href="#"><img alt="" src="http://placehold.it/85x85"></a></td>
                                <td><a href="#"> Product Title</a></td>
                                <td>X3</td>
                                <td>$ 100.00</td>
                                <td><a class="close" href="#"><i class="fa fa-close font-13"></i></a></td>
                              </tr>
                              <tr>
                                <td><a href="#"><img alt="" src="http://placehold.it/85x85"></a></td>
                                <td><a href="#"> Product Title</a></td>
                                <td>X2</td>
                                <td>$ 70.00</td>
                                <td><a class="close" href="#"><i class="fa fa-close font-13"></i></a></td>
                              </tr>
                            </tbody>
                          </table>
                          <div class="total-cart text-right">
                            <table class="table table-responsive">
                              <tbody>
                                <tr>
                                  <td>Cart Subtotal</td>
                                  <td>$170.00</td>
                                </tr>
                                <tr>
                                  <td>Shipping and Handling</td>
                                  <td>$20.00</td>
                                </tr>
                                <tr>
                                  <td>Order Total</td>
                                  <td>$190.00</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="cart-btn text-right">
                              <a class="btn btn-theme-colored btn-xs" href="shop-cart.html"> View cart</a>
                              <a class="btn btn-dark btn-xs" href="shop-checkout.html"> Checkout</a>
                          </div>
                        </div>
                        <!-- dropdown cart ends -->
                      </li>
                    </ul>
                  </div>
                </li>
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-search-box has-dropdown text-white text-hover-theme-colored"><i class="fa fa-search font-13"></i> &nbsp;</a>
                    <ul class="dropdown">
                      <li>
                        <div class="search-form-wrapper">
                          <form method="get" class="mt-10">
                            <input type="text" onfocus="if(this.value =='Enter your search') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Enter your search'; }" value="Enter your search" id="searchinput" name="s" class="">
                            <label><input type="submit" name="submit" value=""></label>
                          </form>
                        </div>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                <!-- <li><a href="#"><i class="fa fa-google-plus text-white"></i></a></li> -->
                <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
                <!-- <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-5">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="{{asset('images/katuumori.png')}}" alt=""></a>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-4 col-md-3 hidden">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-clock-o text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li><a href="#" class="font-12 text-gray text-uppercase">We are open!</a>
                  <h5 class="font-13 text-black m-0"> Mon-Fri 8:00-16:00</h5>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 pull-right">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-phone-square text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-gray text-uppercase">Nomor Telpon</a>
                  <h5 class="font-14 m-0"> 0823-1254-3008</h5>
                </li>
              </ul>
            </div>
          </div>  
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu">
              @if(Auth::guard()->check())
              <li class="{{setActive('home')}}"><a href="{{route('home')}}">Dashboard</a></li>
              <li class="{{setActive('harga-product')}}"><a href="#">Harga Product</a>
                <ul class="dropdown">
                  <li><a href="{{route('harga-product','pulsa')}}">Pulsa Elektrik</a></li>
                  <li><a href="{{route('harga-product','pln')}}">Token Listrik</a></li>
                  <li><a href="{{route('harga-product','game')}}">Voucher Game</a></li>
                  <li><a href="{{route('harga-product','paket-internet')}}">Paket Internet</a></li>
                  <li><a href="{{route('harga-product','paket-sms')}}">Paket SMS</a></li>
                  <li><a href="{{route('harga-product','paket-telpon')}}">Paket Telpon</a></li>
                  <li><a href="{{route('harga-product','saldo-OVO')}}">Saldo OVO</a></li>
                  <li><a href="{{route('harga-product','saldo-Gojek')}}">Saldo Gojek</a></li>
                  <li><a href="{{route('harga-product','saldo-Gojek-Driver')}}">Saldo Gojek Driver</a></li>
                  <li><a href="{{route('harga-product','saldo-Grab')}}">Saldo Grab</a></li>
                  <li><a href="{{route('harga-product','saldo-Grab-Driver')}}">Saldo Grab Driver</a></li>
                  <li><a href="{{route('harga-product','saldo-Linkaja')}}">Saldo Link Aja</a></li>
                  <li><a href="{{route('harga-product','saldo-Shopeepay')}}">Saldo ShopeePay</a></li>
                  <li><a href="{{route('harga-product','wifi-id')}}">Wifi ID</a></li>
                  <li><a href="{{route('harga-product','google-play')}}">Google Play</a></li>
                </ul>
              </li>
														<li class="{{setActive('request-topup')}}"><a href="#">Deposit</a>
                <ul class="dropdown">
																		<li><a href="{{route('request-topup')}}">Request Topup</a></li>
																		<li><a href="{{route('buku-saldo-agen')}}">Buku Saldo Agen</a></li>
																		<li><a href="{{route('topup-manual')}}">Topup Manual</a></li>
                </ul>
														</li>
														<li class="{{setActive('request-transfer')}}"><a href="#">Transfer</a>
                <ul class="dropdown">
																		<li><a href="{{route('request-transfer')}}">Request Transfer</a></li>
																		<li><a href="">Data Transfer</a></li>
                </ul>
														</li>
														<li class="{{setActive('data-transaksi-ppob')}}"><a href="{{route('data-transaksi-ppob')}}">Data Transaksi PPOB</a></li>

														<li class="{{setActive('laporan-transaksi-ppob')}}"><a href="{{route('laporan-transaksi-ppob')}}">Laporan Transaksi PPOB</a></li>
              @endif

            </ul>
            <ul class="pull-right flip hidden-sm hidden-xs">
              <li>
   
                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="{{route('login')}}">Login</a>
                <!-- Modal: Book Now End -->
              </li>
            </ul>
            <div id="top-search-bar" class="collapse">
              <div class="container">
                <form role="search" action="#" class="search_form_top" method="get">
                  <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                  <span class="search-close"><i class="fa fa-search"></i></span>
                </form>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
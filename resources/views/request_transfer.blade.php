@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Request Transfer</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Request Transfer</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Request Transfer</h3>
              </div>
              <div class="panel-body">
              @include('flash::message')
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl Transaksi</th>
                              <th>No. Transaksi</th> 
                              <th>Nominal Transafer</th> 
                              <th>Status</th>
                              <th>Action</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        @foreach($datas as $key => $data)
                            <tr> 
                                <th scope="row">{{$key+1}}</th> 
                                <td>{{$data->tgl_trx}}</td> 
                                <td>{{$data->no_trx}}</td> 
                                <td>{{number_format($data->nominal)}}</td> 
                                <td>{{$data->status}}</td> 
																																@if($data->status =='menunggu')
                                <td><button class="btn btn-primary" onclick="event.preventDefault();
																																		document.getElementById('proses-topup{{$data->id}}').submit();">Proses</button> <button class="btn btn-danger" onclick="event.preventDefault();
																																		document.getElementById('cancel-topup{{$data->id}}').submit();">Cancel</button></td>
																																@elseif($data->status =='berhasil')
																																<td><button class="btn btn-success">Keterangan</button></td>
																																@else
																																<td><button class="btn btn-danger">Alasan</button></td>
																																@endif
																												</tr> 
																												<form action="{{route('proses-topup')}}" method="post" id="proses-topup{{$data->id}}">
																												@csrf
																													<input type="hidden" name="ids" value="{{$data->id}}">
																												</form>
																												<form action="{{route('cancel-topup')}}" method="post" id="cancel-topup">
																												@csrf
																													<input type="hidden" name="ids" value="{{$data->id}}">
																												</form>
																												@endforeach
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
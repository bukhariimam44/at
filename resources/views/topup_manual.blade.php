@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Topup Manual</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Topup Manual</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
														<div class="panel panel-primary">
																<div class="panel-heading">
																		<h3 class="panel-title">Topup Manual</h3>
																</div>
																<div class="panel-body">
																@include('flash::message')
																<form action="{{route('topup-manual')}}" method="post">
																	@csrf
																	<div class="form-group col-md-4">
																	<label for="">Rekening Bank</label>
																	<select name="bank" id="" class="form-control">
																	@foreach($banks as $bank)
																	<option value="{{$bank->id}}">{{$bank->nama}}</option>
																	@endforeach
																	</select>
																		</div>
																		<div class="form-group col-md-4">
																	<label for="">Username</label>
																	<input type="text" name="username" class="form-control">
																		</div>
																		<div class="form-group col-md-4">
																	<label for="">Nominal</label>
																	<input type="text" name="nominal" class="form-control">
																		</div>
																		<div class="form-group col-md-12">
																	<button class="btn btn-primary form-control">PROSES</button>
																		</div>
																	</form>
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
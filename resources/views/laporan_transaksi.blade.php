@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Transaksi PPOB</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Data Transaksi PPOB</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Data Transaksi PPOB</h3>
              </div>
              <div class="panel-body">
														@include('flash::message')
														<form action="{{route('laporan-transaksi-ppob')}}" method="post">
															<input type="hidden" name="action" value="cari">
														@csrf
														<div class="row">
																<div class="col-md-4">
																		<input type="date" class="form-control" name="from" placeholder="From" value="{{$from}}">
																</div>
																<div class="col-md-4">
																		<input type="date" class="form-control" name="to"  placeholder="To" value="{{$to}}">
																</div>
															
																
																<div class="col-md-4">
																		<button type="submit" class="btn btn-primary form-control">Cari</button>
																</div>
														</div>
														</form>
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Nama Client</th>
     
																														<th>Jual</th>
																														<th>Price</th>
																														<th>Untung</th>
																														<th>Sisa Saldo</th>
                            </tr> 
                        </thead> 
                        <tbody> 
																								<?php $tot_jual = 0;
																								$tot_price = 0;
																								$tot_saldo = 0;?>
                        @foreach($datas as $key => $data)
																								<?php $tot_jual+=$data->jual;
																								$tot_price+=$data->price;
																								$saldo = App\User::where('username',$data->username)->first();
																								$tot_saldo+=$saldo->saldo;
																								?>
                            <tr> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{$data->username}}</td> 																														
																																<td>{{number_format($data->jual,0,',','.')}}</td> 
																																<td>{{number_format($data->price,0,',','.')}}</td>
																																<td><span @if($data->jual < $data->price) style="color:red" @endif>{{number_format($data->jual-$data->price,0,',','.')}}</span></td>
																																<td>{{number_format($saldo->saldo,0,',','.')}}</td>
																												</tr> 
																												@endforeach
																												<tr>
																												<td colspan="2">Total</td>
																												<td>{{number_format($tot_jual,0,',','.')}}</td>
																												<td>{{number_format($tot_price,0,',','.')}}</td>
																												<td>{{number_format($tot_jual-$tot_price,0,',','.')}}</td>
																												<td>{{number_format($tot_saldo,0,',','.')}}</td>
																												</tr>
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
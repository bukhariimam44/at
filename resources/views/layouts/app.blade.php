<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content=" KATU'UMORI | Komunitas Arisan Keluarga" />
<meta name="keywords" content="Komunitas Arisan Keluarga" />
<meta name="author" content="Imam" />
<!-- Page Title -->
<title>Katu'umori</title>
<!-- Favicon and Touch Icons -->
<link href="{{asset('images/')}}" rel="shortcut icon" type="image/png">
<link href="{{asset('images/')}}" rel="apple-touch-icon">
<link href="{{asset('images/')}}" rel="apple-touch-icon" sizes="72x72">
<link href="{{asset('images/')}}" rel="apple-touch-icon" sizes="114x114">
<link href="{{asset('images/')}}" rel="apple-touch-icon" sizes="144x144">
<!-- Stylesheet -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/css-plugin-collections.css')}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{asset('css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{asset('css/style-main.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{asset('css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{asset('css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{asset('css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- CSS | Theme Color -->
<link href="{{asset('css/colors/theme-skin-color-set-7.css')}}" rel="stylesheet" type="text/css">
<!-- external javascripts -->
<script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{asset('js/jquery-plugin-collection.js')}}"></script>
@yield('css')
<script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
<meta name="_token" id="token" value="{{csrf_token()}}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body>
    @include('includes.header')
    @yield('content')
    @include('includes.footer')
<script src="{{asset('js/custom.js')}}"></script>
@yield('js')
</body>
</html>
@extends('layouts.app_umum')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Buku Saldo Agen</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Buku Saldo Agen</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Buku Saldo Agen</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
																	<form action="{{route('buku-saldo-agen')}}" method="post">
																	@csrf
																		<div class="row">
																				<div class="col-md-3">
																						<input type="text" name="username" value="{{$username}}" class="form-control" placeholder="Username">
																				</div>
																				<div class="col-md-3">
																					<?php $bulan = [
																						['text'=>'Januari','value'=>'01'],
																						['text'=>'Februari','value'=>'02'],
																						['text'=>'Maret','value'=>'03'],
																						['text'=>'April','value'=>'04'],
																						['text'=>'Mei','value'=>'05'],
																						['text'=>'Juni','value'=>'06'],
																						['text'=>'Juli','value'=>'07'],
																						['text'=>'Agustus','value'=>'08'],
																						['text'=>'September','value'=>'09'],
																						['text'=>'Oktober','value'=>'10'],
																						['text'=>'November','value'=>'11'],
																						['text'=>'Desember','value'=>'12']
																					];?>
																						<select name="bulan" id="" class="form-control">
																						@foreach($bulan as $bln)
																						@if($bln['value'] == date('m'))
																							<option value="{{$bln['value']}}" selected>{{$bln['text']}}</option>
																							@else
																							<option value="{{$bln['value']}}">{{$bln['text']}}</option>
																							@endif
																							@endforeach
																						</select>
																				</div>
																				<div class="col-md-3">
																						<select name="tahun" id="" class="form-control">
																									<option value="2020">2020</option>
																									<option value="2021">2021</option>
																									<option value="2022">2022</option>
																									<option value="2023">2023</option>
																									<option value="2024">2024</option>
																						</select>
																				</div>
																				<div class="col-md-3">
																						<button class="btn btn-primary form-control">Cari</button>
																				</div>
																		</div>
																	</form>
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl <br> No.Trx</th>
                              <th>Debet</th> 
                              <th>Kredit</th>
                              <th>Saldo</th> 
                              <th>Keterangan</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                         @foreach($datas as $key=> $data)
                          <tr> 
                              <td>{{$key+1}}.</td> 
																														<td>{{$data->created_at}}<br>{{$data->no_trx}}</td>
																														@if($data->mutasi == 'Debet') 
																														<td>{{number_format($data->nominal)}}</td> 
																														<td>0</td>
																														@else
																														<td>0</td>
																														<td>{{number_format($data->nominal)}}</td> 
																														@endif
                              <td> <font @if($key == 0) style="color:red;font-weight:bold;" @endif> {{number_format($data->saldo_akhir)}}</font></td> 
                              <td>{{$data->keterangan}}</td> 
																										</tr>
																										@endforeach
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
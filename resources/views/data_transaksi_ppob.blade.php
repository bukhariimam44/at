@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Transaksi PPOB</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Data Transaksi PPOB</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Data Transaksi PPOB</h3>
              </div>
              <div class="panel-body">
														@include('flash::message')
														<form action="{{route('data-transaksi-ppob')}}" method="post">
															<input type="hidden" name="action" value="cari">
														@csrf
														<div class="row">
																<div class="col-md-2">
																		<input type="date" class="form-control" name="from" placeholder="From" value="{{$from}}">
																</div>
																<div class="col-md-2">
																		<input type="date" class="form-control" name="to"  placeholder="To" value="{{$to}}">
																</div>
																<div class="col-md-2">
																		<input type="text" class="form-control" name="no_trx"  placeholder="No Transaksi" value="{{old('no_trx')}}">
																</div>
																<div class="col-md-2">
																		<input type="text" class="form-control" name="phone"  placeholder="No HP/IDPEL" value="{{old('no_trx')}}">
																</div>
																<div class="col-md-2">
																		<select name="status" id="" class="form-control">
																		<option value="">Semua</option>
																			@foreach($statuses as $status)
																			@if($status->id == $stts)
																			<option value="{{$status->id}}" selected>{{$status->name}}</option>
																			@else
																			<option value="{{$status->id}}">{{$status->name}}</option>
																			@endif
																			@endforeach
																		</select>
																</div>
																<div class="col-md-2">
																		<select name="supplier" id="" class="form-control">
																		<?php $suppliers = [
																			[
																				'text'=>'Java H2H',
																				'value'=>'javah2h'
																			],
																			[
																				'text'=>'Portal Pulsa',
																				'value'=>'portalpulsa'
																			],
																			[
																				'text'=>'Lara Kost',
																				'value'=>'larakostpulsa'
																			],
																			
																		];?>
																		<option value="">Semua</option>
																			@foreach($suppliers as $supp)
																			@if($supp['value'] == $supplier)
																			<option value="{{$supp['value']}}" selected>{{$supp['text']}}</option>
																			@else
																			<option value="{{$supp['value']}}">{{$supp['text']}}</option>
																			@endif
																			@endforeach
																		</select>
																</div>
																<div class="col-md-2">
																		<button type="submit" class="btn btn-primary form-control">Cari</button>
																</div>
														</div>
														</form>
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl Transaksi</th>
                              <th>No. Transaksi</th> 
                              <th>HP / ID CUST</th> 
																														<th>Jual</th>
																														<th>Price</th>
																														<th>Untung</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr> 
                        </thead> 
                        <tbody> 
																								<?php $tot_jual = 0;
																								$tot_price = 0;?>
                        @foreach($datas as $key => $data)
																								<?php $tot_jual+=$data->jual;
																								$tot_price+=$data->price;?>
                            <tr> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{$data->created_at}}</td> 
                                <td>{{$data->no_trx}}</td> 
																																<td>{{$data->phone}} / {{$data->idcust}}</td> 
																														
																																<td>{{number_format($data->jual,0,',','.')}}</td> 
																																<td>{{number_format($data->price,0,',','.')}}</td>
																																<td><span @if($data->jual < $data->price) style="color:red" @endif>{{number_format($data->jual-$data->price,0,',','.')}}</span></td>
                                <td>{{$data->statusPpobId->name}}</td> 
																																@if($data->status_ppob_id ==1)
                                <td>
																																<button class="btn btn-warning" onclick="event.preventDefault();
																																		document.getElementById('cek-status{{$data->id}}').submit();">Cek Status</button> 
																																	</td>
																																@elseif($data->status_ppob_id =='4')
																																<td><button class="btn btn-success">Keterangan</button></td>
																																@else
																																<td><button class="btn btn-danger">Alasan</button></td>
																																@endif
																												</tr> 
																												<form action="{{route('cek-status')}}" method="post" id="cek-status{{$data->id}}">
																												@csrf
																													<input type="hidden" name="ids" value="{{$data->id}}">
																												</form>
																												
																												@endforeach
																												<tr>
																												<td colspan="4">Total</td>
																												<td>{{number_format($tot_jual,0,',','.')}}</td>
																												<td>{{number_format($tot_price,0,',','.')}}</td>
																												<td>{{number_format($tot_jual-$tot_price,0,',','.')}}</td>
																												<td colspan="2"></td>
																												</tr>
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
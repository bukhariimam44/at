@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Harga {{$product}}</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Data Harga {{$product}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Data Harga {{$product}} <button style="margin-top:-7px;" class="pull-right btn btn-success" onclick="event.preventDefault();
                document.getElementById('update-harga').submit();">Update</button></h3>
              </div>
              <div class="panel-body">
              @include('flash::message')
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Provider</th>
                              <th>Code</th> 
                              <th>Description</th> 
                              <th>Harga Supplier</th>
																														<th>Markup</th>
																														<th>Jual</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        <form action="{{route('harga-product',$ids)}}" method="post" id="update-harga">
                        @csrf
                          <input type="hidden" name="action" value="update">
                          <input type="hidden" name="provider" value="{{$code}}">
                            @foreach($datas as $key => $data)
                            <input type="hidden" name="ids[]" value="{{$data->id}}">
                            <tr @if($data->status == 'gangguan') style="background-color:yellow;" @endif> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{$data->provider}}</td> 
                                <td>{{$data->code}}</td> 
                                <td>{{$data->description}}</td> 
                                <td>{{number_format($data->price)}}</td> 
                                <td width="100px">
                                    <input type="text" name="markup[]" value="{{number_format($data->markup)}}" class="form-control">
																																</td>
																																<td>{{number_format($data->price + $data->markup)}}</td> 
                            </tr> 
                            @endforeach
                        </form> 
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection